﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]

    public class HeadeDiscountScheme
    {

        [DataMember]
        public string HeaderDisID { get; set; }

        [DataMember]
        public string DID { get; set; }

        [DataMember]
        public string HeaderDisDiscription { get; set; }

        [DataMember]
        public string HeaderDisType { get; set; }

    }
}