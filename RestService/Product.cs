﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class Product
    {
        [DataMember]
        public string StockLink { get; set; }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string QtyOnHand { get; set; }

        [DataMember]
        public string ProductGroup { get; set; }
        [DataMember]
        public string TTI { get; set; }
    }
}