﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class MeterReading
    {

        [DataMember]
        public string MeterReadingID { get; set; }
        [DataMember]
        public string RepID { get; set; }
        [DataMember]
        public string DID { get; set; }
        [DataMember]
        public string MeterDate { get; set; }
        [DataMember]
        public string DayStart { get; set; }
        [DataMember]
        public string DayEnd { get; set; }
        [DataMember]
        public string Additinalusege { get; set; }


    }
}