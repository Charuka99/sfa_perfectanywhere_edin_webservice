﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;


namespace RestService
{
    [DataContract(Namespace = "")]
    public class ReceiptLine
    {
        [DataMember]
        public string RepID { get; set; }

        [DataMember]
        public string fRepID { get; set; }

        [DataMember]
        public string CompID { get; set; }

        [DataMember]
        public string DeviceID { get; set; }

        [DataMember]
        public string AutoID { get; set; }

        [DataMember]
        public string RecID { get; set; }

        [DataMember]
        public string ReceiptHeaderID { get; set; }

        [DataMember]
        public string ReceiptCode { get; set; }

        [DataMember]
        public string InvoiceCode { get; set; }

        [DataMember]
        public string InvoiceDate { get; set; }

        [DataMember]
        public string InvoiceAmount { get; set; }

        [DataMember]
        public string SetOffAmount { get; set; }

        [DataMember]
        public string RecFlag { get; set; }     
    }
}