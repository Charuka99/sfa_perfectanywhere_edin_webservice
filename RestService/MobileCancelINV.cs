﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace RestService
{
     [DataContract(Namespace = "")]
    public class MobileCancelInv
    {
        [DataMember]
        public string invoiceCode { get; set; }
        [DataMember]
        public string SalesType { get; set; }
        [DataMember]
        public string Status { get; set; }

    }
}