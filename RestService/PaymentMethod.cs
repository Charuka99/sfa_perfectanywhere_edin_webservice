﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class PaymentMethod
    {
        [DataMember]
    
       public string PaymentMethodID { get; set; }

        [DataMember]
        public  string PaymentMethodCode { get; set; }

        [DataMember]
        public  string PaymentMethodDescription { get; set; }

    



    }
}