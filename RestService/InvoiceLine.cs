﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class InvoiceLine
    {
        [DataMember]
        public string RepID { get; set; }

        [DataMember]
        public string CompID { get; set; }

        [DataMember]
        public string DeviceID { get; set; }

        [DataMember]
        public string AutoID { get; set; }

        [DataMember]
        public string InvHeaderID { get; set; }

        [DataMember]
        public string InvoiceCode { get; set; }

        [DataMember]
        public string StockLink { get; set; }

        [DataMember]
        public string Qty { get; set; }

        [DataMember]
        public string ProcessedQty { get; set; }

        [DataMember]
        public string UnitExclPrice { get; set; }

        [DataMember]
        public string UnitInclPrice { get; set; }

        [DataMember]
        public string LineDiscPerc { get; set; }

        [DataMember]
        public string LineTaxAmount { get; set; }
        
        [DataMember]
        public string LineExclNoDisc { get; set; }

        [DataMember]
        public string LineInclNoDisc { get; set; }

        [DataMember]
        public string LineExclWithDisc { get; set; }

        [DataMember]
        public string LineInclWithDisc { get; set; }

        [DataMember]
        public string SalesType { get; set; }

        [DataMember]
        public string TrnYear { get; set; }

        [DataMember]
        public string TrnMonth { get; set; }

        [DataMember]
        public string TrnDay { get; set; }

        [DataMember]
        public string itemStockLinkFIRe { get; set; }
        [DataMember]
        public string LotId { get; set; }

        [DataMember]
        public string isLineDiscEdited { get; set; }
        [DataMember]
        public string lineDiscRate { get; set; }


        [DataMember]
        public string taxRate { get; set; }


        [DataMember]
        public string RemarkFlag { get; set; }


        [DataMember]
        public string uomActualid { get; set; }


        [DataMember]
        public string uomTancactionID { get; set; }

        [DataMember]
        public string ItemRemark { get; set; }

        [DataMember]
        public string ItemStatus { get; set; }




    }
}