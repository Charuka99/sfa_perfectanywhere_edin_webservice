﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class SalesRep
    {
        [DataMember]
        public string RepKey { get; set; }

        [DataMember]
        public string RepCode { get; set; }

        [DataMember]
        public string RepName { get; set; }

        [DataMember]
        public string DeviceID { get; set; }

        [DataMember]
        public string RepPassword { get; set; }

        [DataMember]
        public string EMEI { get; set; }

        [DataMember]
        public string RepId { get; set; }

        [DataMember]
        public string ClassCode { get; set; }

        [DataMember]
        public string MaxID { get; set; }

        [DataMember]
        public string MaxIDInv { get; set; }

        [DataMember]
        public string MaxIDRec { get; set; }

        [DataMember]
        public string SalesOrder { get; set; }

        [DataMember]
        public string SalesInvoice { get; set; }

        [DataMember]
        public string SalesInvoicePayment { get; set; }

        [DataMember]
        public string Receipt { get; set; }

        [DataMember]
        public string ImageSalesInvoice { get; set; }

        [DataMember]
        public string ImageReceipt { get; set; }

        [DataMember]
        public string ImageCheque { get; set; }

        [DataMember]
        public string Discount { get; set; }

        [DataMember]
        public string FreeIssue { get; set; }

        [DataMember]
        public string CustomerArea { get; set; }

        [DataMember]
        public string ProductGroup { get; set; }

        [DataMember]
        public string LotTracking { get; set; }

        [DataMember]

        public string MaxIDSR { get; set; }

        [DataMember]
        public string MaxIDMR { get; set; }






        [DataMember]

        public string AllowMaxQtyExceedStockStatusSO { get; set; }
        [DataMember]
        public string AllowMaxQtyExceedreduseStockStatusSI { get; set; }
        [DataMember]
        public string _reduseStockStatusSO { get; set; }
        [DataMember]
        public string _reduseStockStatusSI { get; set; }

        [DataMember]
        public string allowLineDiscount { get; set; }
        [DataMember]
        public string allowheaderDiscount { get; set; }
        [DataMember]
        public string isNeAmounHederDiscount { get; set; }
        [DataMember]
        public string isGrossAmounHeaderDiscount { get; set; }
        [DataMember]
        public string DistributorID { get; set; }


        [DataMember]

        public string IsSendSignature { get; set; }
        [DataMember]

        public string LocationTacking { get; set; }
        [DataMember]

        public string LocTrackingTime { get; set; }
        [DataMember]

        public string AllowNewCusSales { get; set; }
        [DataMember]
        public string AllowSingleSales { get; set; }
        [DataMember]
        public string AllowMulSales { get; set; }
        [DataMember]
        public string AllowCashSales { get; set; }
        [DataMember]
        public string AllowChequeSales { get; set; }
        [DataMember]
        public string AllowCreditSales
        { get; set; }
        [DataMember]

        public string SalesOrderPayement { get; set; }
        [DataMember]
        public string IsFIFO { get; set; }
        [DataMember]
        public string IsBatchPricing
        { get; set; }

        [DataMember]
        public string ISCustomerOutSadingCheck
        { get; set; }

        [DataMember]
        public string isQRcodeApplicable
        { get; set; }

        [DataMember]
        public string isCusCodeApplicable
        { get; set; }

        [DataMember]
        public string ExtratStringfiled1
        { get; set; }

        [DataMember]
        public string nomalfreeIssues
        { get; set; }

        [DataMember]
        public string Multipalefreeissue
        { get; set; }

        [DataMember]
        public string SpecialFreeissue
        { get; set; }


        [DataMember]
        public string IsInvoiceWiseReturn
        { get; set; }

        [DataMember]
        public string IspopUpPDCheque
        { get; set; }

        [DataMember]
        public string showItemDetails
        { get; set; }

        [DataMember]
        public string IsMenuLandScape_rMST
        { get; set; }

        [DataMember]
        public string IsReprint_rMST
        { get; set; }


        [DataMember]
        public string LineFreeIssue_rMST
        { get; set; }

        [DataMember]
        public string ISPrinterL50_rMST
        { get; set; }

        [DataMember]
        public string IsitemLoadCatwise_rMST
        { get; set; }

        [DataMember]
        public string IsKeepItemCatolog_rMST
        { get; set; }


        [DataMember]
        public string BusinessUnitName
        { get; set; }


        [DataMember]
        public string keepPotasialItem
        { get; set; }


        [DataMember]
        public string ISCheckOnlineQty
        { get; set; }

        [DataMember]
        public string ISShowLossOrders
        { get; set; }


        [DataMember]
        public string SkiprouteP
        { get; set; }

        [DataMember]
        public string freeIssueType
        { get; set; }



        public string IsSVAT
        { get; set; }

        [DataMember]
        public string ISVAT
        { get; set; }


        [DataMember]
        public string ISNONVAT
        { get; set; }

        [DataMember]
        public string PrintOptionSelection
        { get; set; }

        [DataMember]
        public string repTelephone
        { get; set; }

        [DataMember]
        public string returnFreeIssueDiscount
        { get; set; }


        [DataMember]
        public string returnshowFreeIssue
        { get; set; }

        [DataMember]
        public string itemRemark
        { get; set; }



        [DataMember]
        public string itemstaus
        { get; set; }


        [DataMember]
        public string LinkSOtoMR
        { get; set; }

        [DataMember]
        public string isahowactualbillvaue
        { get; set; }

        [DataMember]
        public string monthforPotentialitems
        { get; set; }


        [DataMember]
        public string RepRegion
        { get; set; }

        [DataMember]
        public string newcustomerInvoicevalue
        { get; set; }

        [DataMember]
        public string CrieditSOINVLimit
        { get; set; }



        [DataMember]
        public string ShopAuditNum
        { get; set; }


        [DataMember]
        public string MacIdQuo
        { get; set; }
        [DataMember]
        public string IsEnableQuotation
        { get; set; }
        [DataMember]
        public string IsEnableShopAudit
        { get; set; }
        [DataMember]
        public string IsEnableBarcodeHeader
        { get; set; }
        [DataMember]
        public string IsEnableBarcodeLine
        { get; set; }
        [DataMember]
        public string IsConvertToInvoice
        { get; set; }
        [DataMember]
        public string IsShowItemImage
        { get; set; }
        [DataMember]
        public string IsEnableSalesReturn
        { get; set; }
        [DataMember]
        public string IsEnableMarketReturn
        { get; set; }
        [DataMember]
        public string IsEnableBulkReceipt
        { get; set; }
        [DataMember]
        public string IsEnableSaleshistory
        { get; set; }
        [DataMember]
        public string IsEnableReceiptHistory
        { get; set; }
        [DataMember]
        public string IsEnableChangePriceForSale
        { get; set; }
        [DataMember]
        public string IsEnableShowItemBatch
        { get; set; }
    }
}