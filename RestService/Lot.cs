﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace RestService
{
    public class Lot
    {
        [DataMember]
        public string InvoiceNo { get; set; }

        [DataMember]
        public string StockLink { get; set; }

        [DataMember]
        public string LotID { get; set; }

        [DataMember]
        public string Qty { get; set; }

        [DataMember]
        public string ItemStatus { get; set; }
    }
}