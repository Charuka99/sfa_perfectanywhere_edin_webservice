﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace RestService
{
    [DataContract(Namespace = "")]
    public class ProductGroupSequenceByOrder
    {
        [DataMember]
        public string RepID { get; set; }

        [DataMember]
        public string CompanyID { get; set; }

        [DataMember]
        public string CategoryID { get; set; }

        [DataMember]
        public string CategoryName { get; set; }


        [DataMember]
        public string SequenceNumber { get; set; }








    }
}