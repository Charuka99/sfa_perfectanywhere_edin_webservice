﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class MethodResult
    {
        [DataMember]
        public string ReturnID { get; set; }

        [DataMember]
        public string ReturnValue { get; set; }

        [DataMember]
        public string LastID { get; set; }
    }
}