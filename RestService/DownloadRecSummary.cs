﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class DownloadRecSummary
    {
        [DataMember]
        public string NumOfCustomers { get; set; }

        [DataMember]
        public string NumOfProducts { get; set; }

        [DataMember]
        public string NumOfPriceListsPrices { get; set; }
    }
}