﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Hosting;
using System.Globalization;

namespace RestService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RestServiceImpl" in code, svc and config file together.
    public class RestServiceImpl : IRestServiceImpl
    {
        #region IRestService Members

        public string XMLData(string id)
        {
            return "You requested product " + id;
        }


        //public string UploadInvoiceHeader(InvoiceHeader invh)
        //{
        //    var sqlCommand = new SqlCommand();
        //    var sqlConn = new SqlConnection();
        //    string strResult = "OK";

        //    try
        //    {

        //        sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
        //        sqlCommand.CommandType = CommandType.StoredProcedure;
        //        sqlCommand.CommandText = "dbo.Mobile_InvoiceHeaderInsert";

        //        // input param
        //        sqlCommand.Parameters.Add(new SqlParameter("@DeviceID", invh.DeviceID));
        //        sqlCommand.Parameters.Add(new SqlParameter("@DistributorID", invh.CompID));
        //        sqlCommand.Parameters.Add(new SqlParameter("@AutoID", invh.AID));
        //        sqlCommand.Parameters.Add(new SqlParameter("@RecID", invh.RecID));
        //        sqlCommand.Parameters.Add(new SqlParameter("@InvoiceCode", invh.InvoiceCode));
        //        sqlCommand.Parameters.Add(new SqlParameter("@OrderDate", invh.OrderDate));
        //        sqlCommand.Parameters.Add(new SqlParameter("@InvoiceDate", invh.InvoiceDate));
        //        sqlCommand.Parameters.Add(new SqlParameter("@RepID", invh.RepID));
        //        sqlCommand.Parameters.Add(new SqlParameter("@AccountID", invh.AccountID));
        //        sqlCommand.Parameters.Add(new SqlParameter("@ProjectID", invh.ProjectID));
        //        sqlCommand.Parameters.Add(new SqlParameter("@HeaderDiscPerc", invh.HeaderDiscPerc));
        //        sqlCommand.Parameters.Add(new SqlParameter("@ExclNoDisc", invh.ExclNoDisc));
        //        sqlCommand.Parameters.Add(new SqlParameter("@TaxAmount", invh.TaxAmount));
        //        sqlCommand.Parameters.Add(new SqlParameter("@ExclWithDisc", invh.ExclWithDisc));
        //        sqlCommand.Parameters.Add(new SqlParameter("@InclWithDisc", invh.InclWithDisc));
        //        sqlCommand.Parameters.Add(new SqlParameter("@SalesType", invh.SalesType));
        //        sqlCommand.Parameters.Add(new SqlParameter("@NumberOfLine", invh.NumberOfLine));
        //        sqlCommand.Parameters.Add(new SqlParameter("@RecFlag", "1"));
        //        sqlCommand.Parameters.Add(new SqlParameter("@pMethode", invh.pMethode));
        //        sqlCommand.Parameters.Add(new SqlParameter("@latitude", invh.latitude));
        //        sqlCommand.Parameters.Add(new SqlParameter("@longitude", invh.longitude));
        //        sqlCommand.Parameters.Add(new SqlParameter("@INVDate", invh.ProcessedDate));
        //        sqlCommand.Parameters.Add(new SqlParameter("@headerDiscRate", invh.headerDiscRate));
        //        sqlCommand.Parameters.Add(new SqlParameter("@isHeaderDiscEdited", invh.isHeaderDiscEdited));
        //        sqlCommand.Parameters.Add(new SqlParameter("@isQRVerified", invh.isQRVerified));
        //        sqlCommand.Parameters.Add(new SqlParameter("@invoiceRemark", invh.ExtratStringfiled1));
        //        sqlCommand.Parameters.Add(new SqlParameter("@ProcessInvCode", invh.ProcessInvCode));
        //        //sqlCommand.Parameters.Add(new SqlParameter("@HeaderDiscon", invh.HeaderDiscon));

        //        sqlCommand.Parameters.Add(new SqlParameter("@BankName", invh.BankName));
        //        sqlCommand.Parameters.Add(new SqlParameter("@cheque", invh.cheque));
        //        sqlCommand.Parameters.Add(new SqlParameter("@chequeDate", invh.chequeDate));
        //        sqlCommand.Parameters.Add(new SqlParameter("@BatLevel", invh.BatLevel));
        //        sqlCommand.Parameters.Add(new SqlParameter("@EndDatetime", invh.EndDatetime));
        //        sqlCommand.Parameters.Add(new SqlParameter("@linksonumber", invh.linksonumber));
        //        sqlCommand.Parameters.Add(new SqlParameter("@ErlyOutoftheroueRemark", invh.ErlyOutoftheroueRemark));


        //        var returnString = sqlCommand.Parameters.Add("@RetID", SqlDbType.Int);
        //        returnString.Direction = ParameterDirection.Output;

        //        sqlConn.Open();

        //        sqlCommand.Connection = sqlConn;
        //        var intResult = sqlCommand.ExecuteNonQuery();
        //        strResult = returnString.Value.ToString();
        //        sqlConn.Close();
        //        //strResult = "200";
        //        //  file.WriteLine("strResult");
        //    }
        //    catch (Exception ex)
        //    {
        //        strResult = "UploadInvoiceHeader: " + ex.Message;
        //        //  file.WriteLine(strResult);

        //    }
        //    // file.Close();
        //    /* */
        //    return strResult;
        //}

        // invoice heade with lines.
        public string UploadInvoiceHeader(InvoiceHeader invh)
        {
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            string strResult = "OK";
            string strResultLine = "OK";

            int Numberofline = 0;
            int actualLineCount = 0;


            try
            {

                sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "dbo.Mobile_InvoiceHeaderInsert";

                // input param
                sqlCommand.Parameters.Add(new SqlParameter("@DeviceID", invh.DeviceID));
                sqlCommand.Parameters.Add(new SqlParameter("@DistributorID", invh.CompID));
                sqlCommand.Parameters.Add(new SqlParameter("@AutoID", invh.AID));
                sqlCommand.Parameters.Add(new SqlParameter("@RecID", invh.RecID));
                sqlCommand.Parameters.Add(new SqlParameter("@InvoiceCode", invh.InvoiceCode));
                sqlCommand.Parameters.Add(new SqlParameter("@OrderDate", invh.OrderDate));
                sqlCommand.Parameters.Add(new SqlParameter("@InvoiceDate", invh.InvoiceDate));
                sqlCommand.Parameters.Add(new SqlParameter("@RepID", invh.RepID));
                sqlCommand.Parameters.Add(new SqlParameter("@AccountID", invh.AccountID));
                sqlCommand.Parameters.Add(new SqlParameter("@ProjectID", invh.ProjectID));
                sqlCommand.Parameters.Add(new SqlParameter("@HeaderDiscPerc", invh.HeaderDiscPerc));
                sqlCommand.Parameters.Add(new SqlParameter("@ExclNoDisc", invh.ExclNoDisc));
                sqlCommand.Parameters.Add(new SqlParameter("@TaxAmount", invh.TaxAmount));
                sqlCommand.Parameters.Add(new SqlParameter("@ExclWithDisc", invh.ExclWithDisc));
                sqlCommand.Parameters.Add(new SqlParameter("@InclWithDisc", invh.InclWithDisc));
                sqlCommand.Parameters.Add(new SqlParameter("@SalesType", invh.SalesType));
                sqlCommand.Parameters.Add(new SqlParameter("@NumberOfLine", invh.NumberOfLine));
                sqlCommand.Parameters.Add(new SqlParameter("@RecFlag", "1"));
                sqlCommand.Parameters.Add(new SqlParameter("@pMethode", invh.pMethode));
                sqlCommand.Parameters.Add(new SqlParameter("@latitude", invh.latitude));
                sqlCommand.Parameters.Add(new SqlParameter("@longitude", invh.longitude));
                sqlCommand.Parameters.Add(new SqlParameter("@INVDate", invh.ProcessedDate));
                sqlCommand.Parameters.Add(new SqlParameter("@headerDiscRate", invh.headerDiscRate));
                sqlCommand.Parameters.Add(new SqlParameter("@isHeaderDiscEdited", invh.isHeaderDiscEdited));
                sqlCommand.Parameters.Add(new SqlParameter("@isQRVerified", invh.isQRVerified));
                sqlCommand.Parameters.Add(new SqlParameter("@invoiceRemark", invh.ExtratStringfiled1));
                sqlCommand.Parameters.Add(new SqlParameter("@ProcessInvCode", invh.ProcessInvCode));
                //sqlCommand.Parameters.Add(new SqlParameter("@HeaderDiscon", invh.HeaderDiscon));

                sqlCommand.Parameters.Add(new SqlParameter("@BankName", invh.BankName));
                sqlCommand.Parameters.Add(new SqlParameter("@cheque", invh.cheque));
                sqlCommand.Parameters.Add(new SqlParameter("@chequeDate", invh.chequeDate));
                sqlCommand.Parameters.Add(new SqlParameter("@BatLevel", invh.BatLevel));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDatetime", invh.EndDatetime));
                sqlCommand.Parameters.Add(new SqlParameter("@linksonumber", invh.linksonumber));
                sqlCommand.Parameters.Add(new SqlParameter("@ErlyOutoftheroueRemark", invh.ErlyOutoftheroueRemark));

                sqlCommand.Parameters.Add(new SqlParameter("@IspriceListChange", invh.IspriceListChange));

                var returnString = sqlCommand.Parameters.Add("@RetID", SqlDbType.Int);
                returnString.Direction = ParameterDirection.Output;
                sqlConn.Open();

                sqlCommand.Connection = sqlConn;
                var intResult = sqlCommand.ExecuteNonQuery();
                strResult = returnString.Value.ToString();

                Numberofline = Convert.ToInt32(invh.NumberOfLine);
                actualLineCount = 0;
                sqlConn.Close();
                //strResult = "200";
                //  file.WriteLine("strResult");

                if (Convert.ToInt32(strResult) != -1)
                {
                    foreach (InvoiceLine invd in invh.invoiceLines)
                    {

                        try
                        {
                            
                            sqlCommand = new SqlCommand();
                            sqlConn = new SqlConnection();
                            var RecFlag = "1";

                            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
                            sqlCommand.CommandType = CommandType.StoredProcedure;
                            sqlCommand.CommandText = "dbo.Mobile_InvoiceLineInsert";

                            // input param

                            sqlCommand.Parameters.Add(new SqlParameter("@DeviceID", invd.DeviceID));
                            sqlCommand.Parameters.Add(new SqlParameter("@CompID", invd.CompID));
                            sqlCommand.Parameters.Add(new SqlParameter("@AutoID", invd.AutoID));
                            sqlCommand.Parameters.Add(new SqlParameter("@InvHeaderID", Convert.ToInt32(strResult)));
                            sqlCommand.Parameters.Add(new SqlParameter("@InvoiceCode", invd.InvoiceCode));
                            sqlCommand.Parameters.Add(new SqlParameter("@StockLink", invd.StockLink));
                            sqlCommand.Parameters.Add(new SqlParameter("@Qty", invd.Qty));
                            sqlCommand.Parameters.Add(new SqlParameter("@ProcessedQty", invd.ProcessedQty));
                            sqlCommand.Parameters.Add(new SqlParameter("@UnitExclPrice", invd.UnitExclPrice));
                            sqlCommand.Parameters.Add(new SqlParameter("@UnitInclPrice", invd.UnitInclPrice));
                            sqlCommand.Parameters.Add(new SqlParameter("@LineDiscPerc", invd.LineDiscPerc));
                            sqlCommand.Parameters.Add(new SqlParameter("@LineTaxAmount", invd.LineTaxAmount));
                            sqlCommand.Parameters.Add(new SqlParameter("@LineExclNoDisc", invd.LineExclNoDisc));
                            sqlCommand.Parameters.Add(new SqlParameter("@LineInclNoDisc", invd.LineInclNoDisc));
                            sqlCommand.Parameters.Add(new SqlParameter("@LineExclWithDisc", invd.LineExclWithDisc));
                            sqlCommand.Parameters.Add(new SqlParameter("@LineInclWithDisc", invd.LineInclWithDisc));
                            sqlCommand.Parameters.Add(new SqlParameter("@SalesType", invd.SalesType));
                            sqlCommand.Parameters.Add(new SqlParameter("@RecFlag", RecFlag));
                            sqlCommand.Parameters.Add(new SqlParameter("@itemStockLinkFIRe", invd.itemStockLinkFIRe));
                            sqlCommand.Parameters.Add(new SqlParameter("@LotId", invd.LotId));
                            sqlCommand.Parameters.Add(new SqlParameter("@isLineDiscEdited", invd.isLineDiscEdited));
                            sqlCommand.Parameters.Add(new SqlParameter("@lineDiscRate", invd.lineDiscRate));
                            sqlCommand.Parameters.Add(new SqlParameter("@RemarkFlag", invd.RemarkFlag));
                            sqlCommand.Parameters.Add(new SqlParameter("@taxRate", invd.taxRate));
                            sqlCommand.Parameters.Add(new SqlParameter("@uomActualID", invd.uomActualid));
                            sqlCommand.Parameters.Add(new SqlParameter("@uomTrasactionID", invd.uomTancactionID));
                            sqlCommand.Parameters.Add(new SqlParameter("@ItemRemark", invd.ItemRemark));
                            sqlCommand.Parameters.Add(new SqlParameter("@ItemStatus", invd.ItemStatus));

                            sqlCommand.Parameters.Add(new SqlParameter("@ILineCount", actualLineCount));


                            var returnStringLine = sqlCommand.Parameters.Add("@RetID", SqlDbType.Int);
                            returnStringLine.Direction = ParameterDirection.Output;

                            actualLineCount = actualLineCount + 1;

                            sqlConn.Open();

                            sqlCommand.Connection = sqlConn;
                            var intResultLine = sqlCommand.ExecuteNonQuery();
                            strResultLine = returnStringLine.Value.ToString();
                            strResultLine = returnStringLine.Value.ToString();
                            sqlConn.Close();


                            if (Convert.ToInt32(strResultLine) != -1)
                            {

                            }
                            else
                            {
                                strResult = "UploadInvoiceHeader: -1";
                                break;

                                //return "UploadInvoiceHeader: -1";
                            }

                        }
                        catch (Exception ex)
                        {
                            strResultLine = "Line insert: " + ex.Message;
                            strResult = strResultLine;
                            break;
                            //  file.WriteLine(strResult);

                        }
                    }



                    //if (CstrResultLine)
                    //  file.WriteLine("strResult");


                }

            }


            catch (Exception ex)
            {
                strResult = "UploadInvoiceHeader: " + ex.Message;
                //  file.WriteLine(strResult);

            }
            // file.Close();
            /* */
            if (Numberofline > actualLineCount)
            {
                strResult = "Invoice not sent";
            }

            return strResult;
        }


        public string UploadInvoiceDetail(InvoiceLine invd)
        {
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var strResult = "ok";
            //string strPath = HostingEnvironment.MapPath("log.txt");
            var RecFlag = "1";


            try
            {

                sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "dbo.Mobile_InvoiceLineInsert";



                // input param

                sqlCommand.Parameters.Add(new SqlParameter("@DeviceID", invd.DeviceID));
                sqlCommand.Parameters.Add(new SqlParameter("@CompID", invd.CompID));
                sqlCommand.Parameters.Add(new SqlParameter("@AutoID", invd.AutoID));
                sqlCommand.Parameters.Add(new SqlParameter("@InvHeaderID", invd.InvHeaderID));
                sqlCommand.Parameters.Add(new SqlParameter("@InvoiceCode", invd.InvoiceCode));
                sqlCommand.Parameters.Add(new SqlParameter("@StockLink", invd.StockLink));
                sqlCommand.Parameters.Add(new SqlParameter("@Qty", invd.Qty));
                sqlCommand.Parameters.Add(new SqlParameter("@ProcessedQty", invd.ProcessedQty));
                sqlCommand.Parameters.Add(new SqlParameter("@UnitExclPrice", invd.UnitExclPrice));
                sqlCommand.Parameters.Add(new SqlParameter("@UnitInclPrice", invd.UnitInclPrice));
                sqlCommand.Parameters.Add(new SqlParameter("@LineDiscPerc", invd.LineDiscPerc));
                sqlCommand.Parameters.Add(new SqlParameter("@LineTaxAmount", invd.LineTaxAmount));
                sqlCommand.Parameters.Add(new SqlParameter("@LineExclNoDisc", invd.LineExclNoDisc));
                sqlCommand.Parameters.Add(new SqlParameter("@LineInclNoDisc", invd.LineInclNoDisc));
                sqlCommand.Parameters.Add(new SqlParameter("@LineExclWithDisc", invd.LineExclWithDisc));
                sqlCommand.Parameters.Add(new SqlParameter("@LineInclWithDisc", invd.LineInclWithDisc));
                sqlCommand.Parameters.Add(new SqlParameter("@SalesType", invd.SalesType));
                sqlCommand.Parameters.Add(new SqlParameter("@RecFlag", RecFlag));
                sqlCommand.Parameters.Add(new SqlParameter("@itemStockLinkFIRe", invd.itemStockLinkFIRe));
                sqlCommand.Parameters.Add(new SqlParameter("@LotId", invd.LotId));
                sqlCommand.Parameters.Add(new SqlParameter("@isLineDiscEdited", invd.isLineDiscEdited));
                sqlCommand.Parameters.Add(new SqlParameter("@lineDiscRate", invd.lineDiscRate));
                sqlCommand.Parameters.Add(new SqlParameter("@RemarkFlag", invd.RemarkFlag));
                sqlCommand.Parameters.Add(new SqlParameter("@taxRate", invd.taxRate));
                sqlCommand.Parameters.Add(new SqlParameter("@uomActualID", invd.uomActualid));
                sqlCommand.Parameters.Add(new SqlParameter("@uomTrasactionID", invd.uomTancactionID));
                sqlCommand.Parameters.Add(new SqlParameter("@ItemRemark", invd.ItemRemark));
                sqlCommand.Parameters.Add(new SqlParameter("@ItemStatus", invd.ItemStatus));


                // sqlCommand.Parameters.Add(new SqlParameter("@uomActualID",1));
                //  sqlCommand.Parameters.Add(new SqlParameter("@uomTrasactionID", 2));



                var returnString = sqlCommand.Parameters.Add("@RetID", SqlDbType.Int);
                returnString.Direction = ParameterDirection.Output;



                sqlConn.Open();

                sqlCommand.Connection = sqlConn;
                var intResult = sqlCommand.ExecuteNonQuery();
                strResult = returnString.Value.ToString();
                strResult = returnString.Value.ToString();
                sqlConn.Close();
                //  file.WriteLine("strResult");
            }
            catch (Exception ex)
            {
                strResult = "UploadInvoiceDetail: " + ex.Message;
                //  file.WriteLine(strResult);

            }
            // file.Close();
            return strResult;
        }


        public InvoiceLine[] getJsonInvoiceDetail(string DistributorID, string rep)
        {
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "Mobile_InvoiceLineDownLoad";


            sqlCommand.Parameters.Add(new SqlParameter("@DistributorID", DistributorID));
            sqlCommand.Parameters.Add(new SqlParameter("@RepID", rep));

            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            sqlCommand.Dispose();
            sqlConn.Close();

            return (from DataRow row in dt.Rows
                    select new InvoiceLine
                    {

                        InvHeaderID = row["InvoiceHeaderID"].ToString(),
                        InvoiceCode = row["InvoiceCode"].ToString(),
                        StockLink = row["iStockCodeID"].ToString(),
                        Qty = row["fQuantity"].ToString(),
                        ProcessedQty = row["fQtyProcessed"].ToString(),
                        UnitExclPrice = row["fUnitPriceExcl"].ToString(),
                        UnitInclPrice = row["fUnitPriceIncl"].ToString(),
                        LineDiscPerc = row["fLineDiscount"].ToString(),
                        LineTaxAmount = row["fQuantityLineTaxAmount"].ToString(),
                        LineExclNoDisc = row["fQuantityLineTotExclNoDisc"].ToString(),
                        LineInclNoDisc = row["fQuantityLineTotInclNoDisc"].ToString(),
                        LineExclWithDisc = row["fQuantityLineTotExcl"].ToString(),
                        LineInclWithDisc = row["fQuantityLineTotIncl"].ToString(),
                        SalesType = row["DocType"].ToString(),

                        isLineDiscEdited = row["IsLineDiscEdited"].ToString(),
                        lineDiscRate = row["LineDiscRate"].ToString(),
                        RemarkFlag = row["ExtraText1"].ToString(),
                        taxRate = row["fTaxRate"].ToString(),
                        uomActualid = row["uomActualID"].ToString(),
                        uomTancactionID = row["uomTrasactionID"].ToString(),

                    }).ToArray();
        }


        public HeadeDiscountScheme[] getJsonHeadeDiscountScheme(string DistributorID, string rep)
        {
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "Mobile_GetHeadeDiscountScheme";


            sqlCommand.Parameters.Add(new SqlParameter("@DistributorID", DistributorID));
            sqlCommand.Parameters.Add(new SqlParameter("@RepID", rep));

            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            sqlCommand.Dispose();
            sqlConn.Close();

            return (from DataRow row in dt.Rows
                    select new HeadeDiscountScheme
                    {


                        HeaderDisID = row["HeaderDisID"].ToString(),

                        DID = row["DID"].ToString(),

                        HeaderDisDiscription = row["HeaderDisDiscription"].ToString(),

                        HeaderDisType = row["HeaderDisType"].ToString(),

                    }).ToArray();
        }

        public DiscountSchemeList[] getJsonDiscountSchemeList(string DistributorID, string rep)
        {
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "Mobile_GetHeadeDiscountSchemeList";


            sqlCommand.Parameters.Add(new SqlParameter("@DistributorID", DistributorID));
            sqlCommand.Parameters.Add(new SqlParameter("@RepID", rep));

            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            sqlCommand.Dispose();
            sqlConn.Close();

            return (from DataRow row in dt.Rows
                    select new DiscountSchemeList
                    {



                        HeaderDisID = row["HeaderDisID"].ToString(),

                        HeaderDisDetailID = row["HeaderDisDetailID"].ToString(),

                        ItemCatogory = row["ItemCatogory"].ToString(),

                        StockLink = row["StockLink"].ToString(),

                        ItemCode = row["ItemCode"].ToString(),

                        DiscountStockLink = row["DiscountStockLink"].ToString(),

                        DiscountItemCode = row["DiscountItemCode"].ToString(),

                        DiscountItemCatogory = row["DiscountItemCatogory"].ToString(),

                        FromVal = row["FromVal"].ToString(),

                        Toval = row["Toval"].ToString(),

                        DiscountValue = row["DiscountValue"].ToString(),

                        BaseType = row["BaseType"].ToString(),

                        ModifyDate = row["ModifyDate"].ToString(),

                        DID = row["DID"].ToString(),

                        Active = row["Active"].ToString(),

                    }).ToArray();
        }



        public InvoiceHeader[] getJsonInvoiceHeader(string rep, string distributorId)
        {
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "Mobile_InvoiceHeaderDownLoad";
            sqlCommand.Parameters.Add(new SqlParameter("@DistributorID", distributorId));
            sqlCommand.Parameters.Add(new SqlParameter("@RepID", rep));

            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            sqlCommand.Dispose();
            sqlConn.Close();

            return (from DataRow row in dt.Rows
                    select new InvoiceHeader
                    {
                        CompID = row["DistributorID"].ToString(),
                        AutoID = row["InvoiceHeaderID"].ToString(),
                        InvoiceCode = row["InvoiceCode"].ToString(),
                        OrderDate = row["OrderDate"].ToString(),
                        InvoiceDate = row["InvoiceDate"].ToString(),
                        RepID = row["RepID"].ToString(),
                        AccountID = row["AccountID"].ToString(),
                        ProjectID = row["ProjectID"].ToString(),
                        HeaderDiscPerc = row["HeaderDiscPerc"].ToString(),
                        ExclNoDisc = row["ExclWithDisc"].ToString(),
                        InclNoDisc = row["ExclWithDisc"].ToString(),
                        TaxAmount = row["TaxAmount"].ToString(),
                        ExclWithDisc = row["ExclWithDisc"].ToString(),
                        InclWithDisc = row["InclWithDisc"].ToString(),
                        NumberOfLine = row["NumberOfLine"].ToString(),
                        pMethode = row["pMethode"].ToString(),
                        SalesType = row["SalesType"].ToString(),
                        latitude = row["latitude"].ToString(),
                        longitude = row["longitude"].ToString(),
                        ProcessedDate = row["OrderDate"].ToString(),

                        //headerDiscRate = (float)row["headerDiscRate"],
                        //isHeaderDiscEdited = (int)row["isHeaderDiscEdited"]




                    }).ToArray();
        }


        public InvoiceLine[] getJsonInvoiceLine(string distributorId, string rep)
        {
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "Mobile_InvoiceLineDownLoad";


            sqlCommand.Parameters.Add(new SqlParameter("@DistributorID", distributorId));
            sqlCommand.Parameters.Add(new SqlParameter("@RepID", rep));

            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            sqlCommand.Dispose();
            sqlConn.Close();

            return (from DataRow row in dt.Rows
                    select new InvoiceLine
                    {


                        //RepID = row["InvoiceHeaderID"].ToString(),
                        //CompID = row["InvoiceCode"].ToString(),
                        //DeviceID = row["InvoiceCode"].ToString(),
                        //AutoID = row["InvoiceCode"].ToString(),
                        InvHeaderID = row["InvoiceHeaderID"].ToString(),
                        InvoiceCode = row["InvoiceCode"].ToString(),
                        StockLink = row["iStockCodeID"].ToString(),
                        Qty = row["fQuantity"].ToString(),
                        ProcessedQty = row["fQtyProcessed"].ToString(),
                        UnitExclPrice = row["fUnitPriceExcl"].ToString(),
                        UnitInclPrice = row["fUnitPriceIncl"].ToString(),
                        LineDiscPerc = row["fLineDiscount"].ToString(),
                        LineTaxAmount = row["fQuantityLineTaxAmount"].ToString(),
                        LineExclNoDisc = row["fQuantityLineTotExclNoDisc"].ToString(),
                        LineInclNoDisc = row["fQuantityLineTotInclNoDisc"].ToString(),
                        LineExclWithDisc = row["fQuantityLineTotExcl"].ToString(),
                        LineInclWithDisc = row["fQuantityLineTotIncl"].ToString(),
                        SalesType = row["SalesType"].ToString(),
                        // TrnYear = row["InvoiceCode"].ToString(),
                        // TrnMonth = row["InvoiceCode"].ToString(),
                        // TrnDay = row["InvoiceCode"].ToString(),
                        //itemStockLinkFIRe = row["InvoiceCode"].ToString(),
                        LotId = row["InvoiceCode"].ToString(),
                        isLineDiscEdited = row["IsLineDiscEdited"].ToString(),
                        lineDiscRate = row["LineDiscRate"].ToString(),
                        taxRate = row["fTaxRate"].ToString(),
                        //RemarkFlag = row["InvoiceCode"].ToString(),
                        uomActualid = row["uomActualID"].ToString(),
                        uomTancactionID = row["uomTrasactionID"].ToString(),



                    }).ToArray();
        }



        public PriceList[] getJsonPriceList(string DistributorID, string rep)
        {
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "Mobile_PriceList";

            sqlCommand.Parameters.Add(new SqlParameter("@Sel", "1"));
            sqlCommand.Parameters.Add(new SqlParameter("@DistributorID", DistributorID));
            sqlCommand.Parameters.Add(new SqlParameter("@Selection1", rep));
            sqlCommand.Parameters.Add(new SqlParameter("@Selection2", "15"));
            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            sqlCommand.Dispose();
            sqlConn.Close();

            return (from DataRow row in dt.Rows
                    select new PriceList
                    {
                        PriceListID = row["PriceListID"].ToString(),
                        StockLink = row["StockLink"].ToString(),
                        ExclPrice = row["ExclPrice"].ToString(),
                        InclPrice = row["InclPrice"].ToString(),
                        TTIStatus = row["TTIStatus"].ToString(),
                        TaxRateID = row["TaxRateID"].ToString(),
                        LotID = row["LotID"].ToString(),




                    }).ToArray();
        }

        public Customer[] getJsonCustomers(string distributorId, string rep)
        {
            var list = new List<Customer>();

            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "Mobile_Customer";

            sqlCommand.Parameters.Add(new SqlParameter("@Sel", "1"));
            sqlCommand.Parameters.Add(new SqlParameter("@DistributorID", distributorId));
            sqlCommand.Parameters.Add(new SqlParameter("@Selection1", rep));
            sqlCommand.Parameters.Add(new SqlParameter("@Selection2", "15"));
            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            try
            {
                foreach (DataRow row in dt.Rows)
                {
                    string hh = "hhh";
                    var cus = new Customer
                    {
                        AccountID = row[0].ToString(),
                        Code = row[1].ToString(),
                        Name = row[2].ToString(),
                        Address1 = row[3].ToString(),
                        Address2 = row[4].ToString(),
                        Address3 = row[5].ToString(),
                        ClassID = row[6].ToString(),
                        PriceListID = row[7].ToString(),
                        RepID = row[8].ToString(),
                        Outstanding = row[9].ToString(),
                        CreditLimit = row[10].ToString(),
                        CreditDays = row[11].ToString(),
                        Area = row[12].ToString(),
                        ulARTaxStatus = row[13].ToString(),
                        IsFreeIssue = row[14].ToString(),
                        IsDiscount = row[15].ToString(),
                        cLimitStatusSO = row[16].ToString(), //"True") ? "1" : "0",
                        cPeriodStatusSO = row[17].ToString(),// "True") ? "1" : "0",
                        availableQtyConStatusSO = row[18].ToString(),// "True") ? "1" : "0",
                        dPaymentStatusSO = row[19].ToString(),// "True") ? "1" : "0",
                        recieptAvailableSO = row[20].ToString(),// "True") ? "1" : "0",
                        cLimitStatusSI = row[21].ToString(),// "True") ? "1" : "0",
                        cPeriodStatusSI = row[22].ToString(),///, "True") ? "1" : "0",
                        availableQtyConStatusSI = row[23].ToString(),//, "True") ? "1" : "0",
                        dPaymentStatusSI = row[24].ToString(),//, "True") ? "1" : "0",
                        recieptAvailableSI = row[25].ToString(),//, "True") ? "1" : "0",
                        creditPeriod = row[26].ToString(),
                        latitude = row[27].ToString(),
                        longitude = row[28].ToString(),
                        isNewCustomer = row["IsNewcustomer"].ToString(),
                        fIListID = row[30].ToString(),
                        OutsatingOverDue = row[31].ToString(),
                        Rootpirority = row["Rootpirority"].ToString(),
                        PDChequegracePeriod = row["PDChequegracePeriod"].ToString(),
                        contact = row["contact"].ToString(),
                        PaymentType = row["PaymentType"].ToString(),
                        Ishold = row["Ishold"].ToString(),
                        TAXNumber = row["TAXNumber"].ToString(),
                        TAXStuts = row["TAXStuts"].ToString(),
                        customerType = row["customerType"].ToString()


                    };
                    list.Add(cus);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


            sqlCommand.Dispose();
            sqlConn.Close();

            return list.ToArray();

            //return "gf";
        }


        public Customer[] getJsonNewCustomers(string distributorId, string rep)
        {
            var list = new List<Customer>();

            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "Mobile_NewCustomer";

            sqlCommand.Parameters.Add(new SqlParameter("@Sel", "1"));
            sqlCommand.Parameters.Add(new SqlParameter("@DistributorID", distributorId));
            sqlCommand.Parameters.Add(new SqlParameter("@Selection1", rep));
            sqlCommand.Parameters.Add(new SqlParameter("@Selection2", "15"));
            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                string hh = "hhh";
                var cus = new Customer
                {
                    AccountID = row[0].ToString(),
                    Code = row[1].ToString(),
                    Name = row[2].ToString(),
                    Address1 = row[3].ToString(),
                    Address2 = row[4].ToString(),
                    Address3 = row[5].ToString(),
                    ClassID = row[6].ToString(),
                    PriceListID = row[7].ToString(),
                    RepID = row[8].ToString(),
                    Outstanding = row[9].ToString(),
                    CreditLimit = row[10].ToString(),
                    CreditDays = row[11].ToString(),
                    Area = row[12].ToString(),
                    ulARTaxStatus = row[13].ToString(),
                    IsFreeIssue = row[14].ToString(),
                    IsDiscount = row[15].ToString(),
                    cLimitStatusSO = row[16].ToString(), //"True") ? "1" : "0",
                    cPeriodStatusSO = row[17].ToString(),// "True") ? "1" : "0",
                    availableQtyConStatusSO = row[18].ToString(),// "True") ? "1" : "0",
                    dPaymentStatusSO = row[19].ToString(),// "True") ? "1" : "0",
                    recieptAvailableSO = row[20].ToString(),// "True") ? "1" : "0",
                    cLimitStatusSI = row[21].ToString(),// "True") ? "1" : "0",
                    cPeriodStatusSI = row[22].ToString(),///, "True") ? "1" : "0",
                    availableQtyConStatusSI = row[23].ToString(),//, "True") ? "1" : "0",
                    dPaymentStatusSI = row[24].ToString(),//, "True") ? "1" : "0",
                    recieptAvailableSI = row[25].ToString(),//, "True") ? "1" : "0",
                    creditPeriod = row[26].ToString(),
                    latitude = row[27].ToString(),
                    longitude = row[28].ToString(),
                    isNewCustomer = row[29].ToString(),
                    fIListID = row[30].ToString(),
                    OutsatingOverDue = row[31].ToString(),
                    Rootpirority = row["Rootpirority"].ToString(),
                    PDChequegracePeriod = row["PDChequegracePeriod"].ToString()






                };
                list.Add(cus);
            }

            sqlCommand.Dispose();
            sqlConn.Close();

            return list.ToArray();

            //return "gf";
        }


        public CompanyDetails[] getJsonCompanyDetails(string distributorId, string salesRep)
        {
            var list = new List<CompanyDetails>();

            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "Mobile_CompanyDetails";


            sqlCommand.Parameters.Add(new SqlParameter("@DistributorID", distributorId));
            sqlCommand.Parameters.Add(new SqlParameter("@salesRep", salesRep));


            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                string hh = "hhh";
                var _objcompany = new CompanyDetails
                {
                    CompanyID = row[0].ToString(),
                    CompanyName = row[1].ToString(),
                    AddressLine1 = row[2].ToString(),
                    AddressLine2 = row[3].ToString(),
                    AddressLine3 = row[4].ToString(),
                    VATRegNo = row[5].ToString(),
                    ExtraText = row[6].ToString(),
                    ContactNo = row[7].ToString(),
                    FaxNO = row[8].ToString(),
                    ExtraText1 = row[9].ToString(),
                    ExtraText2 = row[10].ToString(),
                    ExtraText3 = row[11].ToString(),
                    RepID = row[12].ToString(),
                    ComDistributorID = row[13].ToString(),
                    ComDistributorName = row[14].ToString(),
                    ComDistributorContact = row[15].ToString(),
                    ComDistributorTown = row[16].ToString(),
                    IsAddDistributorName = row[17].ToString(),
                    IsAddDistributorContact = row[18].ToString(),
                    IsAddTaxFilelds = row[19].ToString(),
                    IsAddFooterText = row[20].ToString(),
                    FooterText = row[21].ToString(),
                    IsAddRuberStamp = row[22].ToString(),

                };
                list.Add(_objcompany);
            }

            sqlCommand.Dispose();
            sqlConn.Close();

            return list.ToArray();

            //return "gf";
        }

        public Product[] getJsonProducts(string comp, string RepID)
        {
            var list = new List<Product>();

            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "Mobile_StockItemSelect";

            sqlCommand.Parameters.Add(new SqlParameter("@Sel", "1"));
            sqlCommand.Parameters.Add(new SqlParameter("@CompanyID", comp));
            sqlCommand.Parameters.Add(new SqlParameter("@Selection1", RepID));
            sqlCommand.Parameters.Add(new SqlParameter("@Selection2", "10"));
            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                var prod = new Product
                {
                    StockLink = row[0].ToString(),
                    Code = row[1].ToString(),
                    Description = row[2].ToString(),
                    QtyOnHand = row[3].ToString(),
                    ProductGroup = row[4].ToString(),
                    TTI = row[5].ToString()
                };
                list.Add(prod);
            }

            sqlCommand.Dispose();
            sqlConn.Close();

            return list.ToArray();
        }

        public ProductWithLot[] getJsonProductsWithLot(string DistributorID, string rep)
        {
            var list = new List<ProductWithLot>();

            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "Mobile_StockItemWithLot";

            sqlCommand.Parameters.Add(new SqlParameter("@Sel", "1"));
            sqlCommand.Parameters.Add(new SqlParameter("@DistributorID", DistributorID));
            sqlCommand.Parameters.Add(new SqlParameter("@Selection1", rep));
            sqlCommand.Parameters.Add(new SqlParameter("@Selection2", "10"));
            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                var prod = new ProductWithLot
                {
                    TransferDate = row[0].ToString(),
                    TransferDescription = row[1].ToString(),
                    TransferReference = row[2].ToString(),
                    StockLink = row[3].ToString(),
                    Code = row[4].ToString(),
                    Description = row[5].ToString(),
                    LotID = row[6].ToString(),
                    LotDescription = row[7].ToString(),
                    ExpiryDate = row[8].ToString(),
                    WarehouseID = row[9].ToString(),
                    QtyOnHand = row[10].ToString(),
                    ProductGroup = row[11].ToString(),
                    TTI = row["TTI"].ToString(),
                    ServiceItem = row["ServiceItem"].ToString(),

                    SerialItem = row["SerialItem"].ToString(),



                    WhseItem = row["WhseItem"].ToString(),


                    iLotStatus = row["iLotStatus"].ToString(),
                    isSellableItem = row["isSellableItem"].ToString(),
                    actualUOM = row["actualUOM"].ToString(),

                    SearchCode = row["SearchCode"].ToString(),
                    CaseMakeUp = row["CaseMakeUp"].ToString(),
                    ItemSequence = row["ItemSequence"].ToString(),

                    Barcode = row["Barcode"].ToString(),
                    
                };
                list.Add(prod);
            }

            sqlCommand.Dispose();
            sqlConn.Close();


            return list.ToArray();
        }

        public OutstandingInvoice[] getJsonOutstandingInvoices(string comp, string rep)
        {
            var list = new List<OutstandingInvoice>();

            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "MobileOutstandingInvoice_Select";

            sqlCommand.Parameters.Add(new SqlParameter("@Sel", "1"));
            sqlCommand.Parameters.Add(new SqlParameter("@CompID", comp));
            sqlCommand.Parameters.Add(new SqlParameter("@RepID", rep));
            sqlCommand.Parameters.Add(new SqlParameter("@InvNumKey", "1"));
            sqlCommand.Parameters.Add(new SqlParameter("@InvoiceCode", "1"));
            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                var inv = new OutstandingInvoice
                {
                    InvID = row[0].ToString(),
                    InvCode = row[1].ToString(),
                    AccountID = row[2].ToString(),
                    CusCode = row[3].ToString(),
                    CusName = row[4].ToString(),
                    InvoiceDate = row[5].ToString(),
                    NetAmount = row[6].ToString(),
                    Outstanding = row[7].ToString(),
                    OrderNo = row[8].ToString(),
                    MobileInvCode = row[9].ToString(),
                    CompID = row[10].ToString()

                };
                list.Add(inv);
            }

            sqlCommand.Dispose();
            sqlConn.Close();

            return list.ToArray();
        }

        public SalesRep getJsonSalesRep(string loginName)
        {
            var rep = new SalesRep();

            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "dbo.Mobile_SalesRep";

            sqlCommand.Parameters.Add(new SqlParameter("@Sel", "1"));
            sqlCommand.Parameters.Add(new SqlParameter("@CompanyID", "1"));
            sqlCommand.Parameters.Add(new SqlParameter("@LoginName", loginName));
            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    rep.RepKey = row[0].ToString();
                    rep.RepCode = row[1].ToString();
                    rep.RepName = row[2].ToString();
                    rep.DeviceID = row[3].ToString();
                    rep.RepPassword = row[4].ToString();
                    rep.EMEI = row[5].ToString();
                    rep.RepId = row[6].ToString();
                    rep.ClassCode = row[7].ToString();
                    rep.MaxID = row[8].ToString();
                    rep.MaxIDInv = row[9].ToString();
                    rep.MaxIDRec = row[10].ToString();
                    rep.SalesOrder = row[11].ToString();
                    rep.SalesInvoice = row[12].ToString();
                    rep.SalesInvoicePayment = row[13].ToString();
                    rep.Receipt = row[14].ToString();
                    rep.ImageSalesInvoice = row[15].ToString();
                    rep.ImageReceipt = row[16].ToString();
                    rep.ImageCheque = row[17].ToString();
                    rep.Discount = row[18].ToString();
                    rep.FreeIssue = row[19].ToString();
                    rep.CustomerArea = row[20].ToString();
                    rep.ProductGroup = row[21].ToString();
                    rep.LotTracking = row[22].ToString();
                    rep.MaxIDSR = row[23].ToString();
                    rep.MaxIDMR = row[24].ToString();
                    rep.AllowMaxQtyExceedStockStatusSO = row[25].ToString();
                    rep.AllowMaxQtyExceedreduseStockStatusSI = row[26].ToString();
                    rep._reduseStockStatusSO = row[27].ToString();
                    rep._reduseStockStatusSI = row[28].ToString();
                    rep.allowheaderDiscount = row[29].ToString();
                    rep.allowLineDiscount = row[30].ToString();
                    rep.isGrossAmounHeaderDiscount = row[31].ToString();
                    rep.isNeAmounHederDiscount = row[32].ToString();
                    rep.DistributorID = row[33].ToString();
                    rep.IsSendSignature = row[34].ToString();
                    rep.LocationTacking = row[35].ToString();
                    rep.LocTrackingTime = row[36].ToString();
                    rep.AllowNewCusSales = row[37].ToString();
                    rep.AllowSingleSales = row[38].ToString();
                    rep.AllowMulSales = row[39].ToString();
                    rep.AllowCashSales = row[40].ToString();
                    rep.AllowChequeSales = row[41].ToString();
                    rep.AllowCreditSales = row[42].ToString();
                    rep.SalesOrderPayement = row[43].ToString();
                    rep.IsFIFO = row[44].ToString();
                    rep.IsBatchPricing = row[45].ToString();
                    if (row[46].ToString() == "1")
                    {
                        rep.ISCustomerOutSadingCheck = row[46].ToString();
                    }
                    else
                    {
                        rep.ISCustomerOutSadingCheck = "0";
                    }

                    if (row[47].ToString() == "1")
                    {
                        rep.isQRcodeApplicable = row[47].ToString();
                    }
                    else
                    {
                        rep.isQRcodeApplicable = "0";
                    }

                    if (row[48].ToString() == "1")
                    {
                        rep.isCusCodeApplicable = row[48].ToString();
                    }
                    else
                    {
                        rep.isCusCodeApplicable = "0";
                    }

                    if (row[49].ToString() == "1")
                    {
                        rep.ExtratStringfiled1 = row[49].ToString();
                    }
                    else
                    {
                        rep.ExtratStringfiled1 = "0";
                    }

                    /////////////////////////////////------------------------------------------------------
                    if (row[50].ToString() == "1")
                    {
                        rep.nomalfreeIssues = row[50].ToString();
                    }
                    else
                    {
                        rep.nomalfreeIssues = "0";
                    }

                    if (row[51].ToString() == "1")
                    {
                        rep.Multipalefreeissue = row[51].ToString();
                    }
                    else
                    {
                        rep.Multipalefreeissue = "0";
                    }

                    if (row[52].ToString() == "1")
                    {
                        rep.SpecialFreeissue = row[52].ToString();
                    }
                    else
                    {
                        rep.SpecialFreeissue = "0";
                    }

                    if (row[53].ToString() == "1")
                    {
                        rep.IsInvoiceWiseReturn = row[53].ToString();
                    }
                    else
                    {
                        rep.IsInvoiceWiseReturn = "0";
                    }

                    if (row[54].ToString() == "1")
                    {
                        rep.IspopUpPDCheque = row[54].ToString();
                    }
                    else
                    {
                        rep.IspopUpPDCheque = "0";
                    }



                    if (row[55].ToString() == "1")
                    {
                        rep.showItemDetails = row[55].ToString();
                    }
                    else
                    {
                        rep.showItemDetails = "0";
                    }

                    if (row[56].ToString() == "1")
                    {
                        rep.IsMenuLandScape_rMST = row[56].ToString();
                    }
                    else
                    {
                        rep.IsMenuLandScape_rMST = "0";
                    }

                    if (row[57].ToString() == "1")
                    {
                        rep.IsReprint_rMST = row[57].ToString();
                    }
                    else
                    {
                        rep.IsReprint_rMST = "0";
                    }

                    if (row[58].ToString() == "1")
                    {
                        rep.LineFreeIssue_rMST = row[58].ToString();
                    }
                    else
                    {

                        if (row[58].ToString() == "100")
                        {
                            rep.LineFreeIssue_rMST = row[58].ToString();
                        }
                        else
                        {
                            rep.LineFreeIssue_rMST = "0";
                        }



                    }

                    if (row[59].ToString() == "1")
                    {
                        rep.ISPrinterL50_rMST = row[59].ToString();
                    }
                    else
                    {
                        rep.ISPrinterL50_rMST = "0";
                    }

                    if (row[60].ToString() == "1")
                    {
                        rep.IsitemLoadCatwise_rMST = row[60].ToString();
                    }
                    else
                    {
                        rep.IsitemLoadCatwise_rMST = "0";
                    }

                    if (row[61].ToString() == "1")
                    {
                        rep.IsKeepItemCatolog_rMST = row[61].ToString();
                    }
                    else
                    {
                        rep.IsKeepItemCatolog_rMST = "0";
                    }

                    //---------------------------------------------------


                    if (row[62].ToString() == "1")
                    {
                        rep.keepPotasialItem = row[62].ToString();
                    }
                    else
                    {
                        rep.keepPotasialItem = "0";
                    }

                    if (row[63].ToString() == "1")
                    {
                        rep.ISCheckOnlineQty = row[63].ToString();
                    }
                    else
                    {
                        rep.ISCheckOnlineQty = "0";
                    }
                    if (row[64].ToString() == "1")
                    {
                        rep.ISShowLossOrders = row[64].ToString();
                    }
                    else
                    {
                        rep.ISShowLossOrders = "0";

                    }


                    if (row[65].ToString() == "1")
                    {
                        rep.SkiprouteP = row[65].ToString();
                    }
                    else
                    {
                        rep.SkiprouteP = "0";

                    }


                    if (row[66].ToString() == "Old")
                    {
                        rep.freeIssueType = row[66].ToString();
                    }
                    else
                    {
                        rep.freeIssueType = "New";

                    }


                    if (row[67].ToString() == "1")
                    {
                        rep.IsSVAT = row[67].ToString();
                    }
                    else
                    {
                        rep.IsSVAT = "0";

                    }

                    if (row[68].ToString() == "1")
                    {
                        rep.ISVAT = row[68].ToString();
                    }
                    else
                    {
                        rep.ISVAT = "0";

                    }

                    if (row[69].ToString() == "1")
                    {
                        rep.ISNONVAT = row[69].ToString();
                    }
                    else
                    {
                        rep.ISNONVAT = "0";

                    }

                    if (row[70].ToString() != "0")
                    {
                        rep.PrintOptionSelection = row[70].ToString();
                    }
                    else
                    {
                        rep.PrintOptionSelection = "0";

                    }


                    if (row[71].ToString() != "")
                    {
                        rep.repTelephone = row[71].ToString();
                    }
                    else
                    {
                        rep.repTelephone = "0";

                    }
                    if (row[72].ToString() != "")
                    {
                        rep.returnFreeIssueDiscount = row[72].ToString();
                    }
                    else
                    {
                        rep.returnFreeIssueDiscount = "0";

                    }

                    if (row[73].ToString() != "")
                    {
                        rep.returnshowFreeIssue = row[73].ToString();
                    }
                    else
                    {
                        rep.returnshowFreeIssue = "0";

                    }

                    if (row[74].ToString() != "")
                    {
                        rep.itemRemark = row[74].ToString();
                    }
                    else
                    {
                        rep.itemRemark = "0";

                    }

                    if (row["itemstaus"].ToString() != "")
                    {
                        rep.itemstaus = row["itemstaus"].ToString();
                    }
                    else
                    {
                        rep.itemstaus = "0";

                    }

                    if (row["LinkSOtoMR"].ToString() != "")
                    {
                        rep.LinkSOtoMR = row["LinkSOtoMR"].ToString();
                    }
                    else
                    {
                        rep.itemstaus = "0";

                    }

                    if (row["isahowactualbillvaue"].ToString() != "")
                    {
                        rep.isahowactualbillvaue = row["isahowactualbillvaue"].ToString();
                    }
                    else
                    {
                        rep.isahowactualbillvaue = "0";

                    }


                    // 2020-10-16 

                    if (row["monthforPotentialitems"].ToString() != "")
                    {
                        rep.monthforPotentialitems = row["monthforPotentialitems"].ToString();
                    }
                    else
                    {
                        rep.monthforPotentialitems = "0";

                    }
                    //2021-02-02

                    if (row["newcustomerInvoicevalue"].ToString() != "")
                    {
                        rep.newcustomerInvoicevalue = row["newcustomerInvoicevalue"].ToString();
                    }
                    else
                    {
                        rep.newcustomerInvoicevalue = "0";

                    }

                    if (row["CrieditSOINVLimit"].ToString() != "")
                    {
                        rep.CrieditSOINVLimit = row["CrieditSOINVLimit"].ToString();
                    }
                    else
                    {
                        rep.CrieditSOINVLimit = "0";

                    }

                    if (row["RepRegion"].ToString() != "")
                    {
                        rep.RepRegion = row["RepRegion"].ToString();
                    }
                    else
                    {
                        rep.RepRegion = "0";

                    }

                    if (row["ShopAuditNum"].ToString() != "")
                    {
                        rep.ShopAuditNum = row["ShopAuditNum"].ToString();
                    }
                    else
                    {
                        rep.ShopAuditNum = "0";

                    }






                    //---------------------------------------------------

                    //---------------------------------------------------------------------------

                    if (row["MacIdQuo"].ToString() != "")
                    {
                        rep.MacIdQuo = row["MacIdQuo"].ToString();
                    }
                    else
                    {
                        rep.MacIdQuo = "0";
                    }

                    if (row["IsEnableQuotation"].ToString() != "")
                    {
                        rep.IsEnableQuotation = row["IsEnableQuotation"].ToString();
                    }
                    else
                    {
                        rep.IsEnableQuotation = "0";
                    }

                    if (row["IsEnableShopAudit"].ToString() != "")
                    {
                        rep.IsEnableShopAudit = row["IsEnableShopAudit"].ToString();
                    }
                    else
                    {
                        rep.IsEnableShopAudit = "0";
                    }

                    if (row["IsEnableBarcodeHeader"].ToString() != "")
                    {
                        rep.IsEnableBarcodeHeader = row["IsEnableBarcodeHeader"].ToString();
                    }
                    else
                    {
                        rep.IsEnableBarcodeHeader = "0";
                    }

                    if (row["IsEnableBarcodeLine"].ToString() != "")
                    {
                        rep.IsEnableBarcodeLine = row["IsEnableBarcodeLine"].ToString();
                    }
                    else
                    {
                        rep.IsEnableBarcodeLine = "0";
                    }

                    if (row["IsConvertToInvoice"].ToString() != "")
                    {
                        rep.IsConvertToInvoice = row["IsConvertToInvoice"].ToString();
                    }
                    else
                    {
                        rep.IsConvertToInvoice = "0";
                    }


                    if (row["IsShowItemImage"].ToString() != "")
                    {
                        rep.IsShowItemImage = row["IsShowItemImage"].ToString();
                    }
                    else
                    {
                        rep.IsShowItemImage = "0";
                    }
                    if (row["IsEnableSalesReturn"].ToString() != "")
                    {
                        rep.IsEnableSalesReturn = row["IsEnableSalesReturn"].ToString();
                    }
                    else
                    {
                        rep.IsEnableSalesReturn = "0";
                    }
                    if (row["IsEnableMarketReturn"].ToString() != "")
                    {
                        rep.IsEnableMarketReturn = row["IsEnableMarketReturn"].ToString();
                    }
                    else
                    {
                        rep.IsEnableMarketReturn = "0";
                    }
                    if (row["IsEnableBulkReceipt"].ToString() != "")
                    {
                        rep.IsEnableBulkReceipt = row["IsEnableBulkReceipt"].ToString();
                    }
                    else
                    {
                        rep.IsEnableBulkReceipt = "0";
                    }
                    if (row["IsEnableSaleshistory"].ToString() != "")
                    {
                        rep.IsEnableSaleshistory = row["IsEnableSaleshistory"].ToString();
                    }
                    else
                    {
                        rep.IsEnableSaleshistory = "0";
                    }
                    if (row["IsEnableReceiptHistory"].ToString() != "")
                    {
                        rep.IsEnableReceiptHistory = row["IsEnableReceiptHistory"].ToString();
                    }
                    else
                    {
                        rep.IsEnableReceiptHistory = "0";
                    }
                    if (row["IsEnableChangePriceForSale"].ToString() != "")
                    {
                        rep.IsEnableChangePriceForSale = row["IsEnableChangePriceForSale"].ToString();
                    }
                    else
                    {
                        rep.IsEnableChangePriceForSale = "0";
                    }
                    if (row["IsEnableShowItemBatch"].ToString() != "")
                    {
                        rep.IsEnableShowItemBatch = row["IsEnableShowItemBatch"].ToString();
                    }
                    else
                    {
                        rep.IsEnableShowItemBatch = "0";
                    }


                }
            }
            else
            {
                rep.RepKey = "0";
                rep.RepCode = "INVALID";
                rep.RepName = "RepCode Not Exists";
            }

            sqlCommand.Dispose();
            sqlConn.Close();

            return rep;
        }

        public SalesRep[] getJsonRepDetails(string comp, string repID)
        {
            var rep1 = new SalesRep();
            var listrep = new List<SalesRep>();

            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "dbo.Mobile_SalesRepDetails";

            sqlCommand.Parameters.Add(new SqlParameter("@Sel", "1"));
            sqlCommand.Parameters.Add(new SqlParameter("@CompanyID", "1"));
            sqlCommand.Parameters.Add(new SqlParameter("@rep", repID));
            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    rep1.RepKey = row[0].ToString();
                    rep1.RepCode = row[1].ToString();
                    rep1.RepName = row[2].ToString();
                    rep1.DeviceID = row[3].ToString();
                    rep1.RepPassword = row[4].ToString();
                    rep1.EMEI = row[5].ToString();
                    rep1.RepId = row[6].ToString();
                    rep1.ClassCode = row[7].ToString();
                    rep1.MaxID = row[8].ToString();
                    rep1.MaxIDInv = row[9].ToString();
                    rep1.MaxIDRec = row[10].ToString();
                    rep1.SalesOrder = row[11].ToString();
                    rep1.SalesInvoice = row[12].ToString();
                    rep1.SalesInvoicePayment = row[13].ToString();
                    rep1.Receipt = row[14].ToString();
                    rep1.ImageSalesInvoice = row[15].ToString();
                    rep1.ImageReceipt = row[16].ToString();
                    rep1.ImageCheque = row[17].ToString();
                    rep1.Discount = row[18].ToString();
                    rep1.FreeIssue = row[19].ToString();
                    rep1.CustomerArea = row[20].ToString();
                    rep1.ProductGroup = row[21].ToString();
                    rep1.LotTracking = row[22].ToString();
                    rep1.MaxIDSR = row[23].ToString();
                    rep1.MaxIDMR = row[24].ToString();
                    rep1.AllowMaxQtyExceedStockStatusSO = row[25].ToString();
                    rep1.AllowMaxQtyExceedreduseStockStatusSI = row[26].ToString();
                    rep1._reduseStockStatusSO = row[27].ToString();
                    rep1._reduseStockStatusSI = row[28].ToString();
                    rep1.allowheaderDiscount = row[29].ToString();
                    rep1.allowLineDiscount = row[30].ToString();
                    rep1.isGrossAmounHeaderDiscount = row[31].ToString();
                    rep1.isNeAmounHederDiscount = row[32].ToString();
                    rep1.DistributorID = row[33].ToString();
                    rep1.IsSendSignature = row[34].ToString();
                    rep1.LocationTacking = row[35].ToString();
                    rep1.LocTrackingTime = row[36].ToString();
                    rep1.AllowNewCusSales = row[37].ToString();
                    rep1.AllowSingleSales = row[38].ToString();
                    rep1.AllowMulSales = row[39].ToString();
                    rep1.AllowCashSales = row[40].ToString();
                    rep1.AllowChequeSales = row[41].ToString();
                    rep1.AllowCreditSales = row[42].ToString();
                    rep1.SalesOrderPayement = row[43].ToString();
                    rep1.IsFIFO = row[44].ToString();
                    rep1.IsBatchPricing = row[45].ToString();
                    if (row[46].ToString() == "1")
                    {
                        rep1.ISCustomerOutSadingCheck = row[46].ToString();
                    }
                    else
                    {
                        rep1.ISCustomerOutSadingCheck = "0";
                    }

                    if (row[47].ToString() == "1")
                    {
                        rep1.isQRcodeApplicable = row[47].ToString();
                    }
                    else
                    {
                        rep1.isQRcodeApplicable = "0";
                    }

                    if (row[48].ToString() == "1")
                    {
                        rep1.isCusCodeApplicable = row[48].ToString();
                    }
                    else
                    {
                        rep1.isCusCodeApplicable = "0";
                    }

                    if (row[49].ToString() == "1")
                    {
                        rep1.ExtratStringfiled1 = row[49].ToString();
                    }
                    else
                    {
                        rep1.ExtratStringfiled1 = "0";
                    }

                    /////////////////////////////////------------------------------------------------------
                    if (row[50].ToString() == "1")
                    {
                        rep1.nomalfreeIssues = row[50].ToString();
                    }
                    else
                    {
                        rep1.nomalfreeIssues = "0";
                    }

                    if (row[51].ToString() == "1")
                    {
                        rep1.Multipalefreeissue = row[51].ToString();
                    }
                    else
                    {
                        rep1.Multipalefreeissue = "0";
                    }

                    if (row[52].ToString() == "1")
                    {
                        rep1.SpecialFreeissue = row[52].ToString();
                    }
                    else
                    {
                        rep1.SpecialFreeissue = "0";
                    }

                    if (row[53].ToString() == "1")
                    {
                        rep1.IsInvoiceWiseReturn = row[53].ToString();
                    }
                    else
                    {
                        rep1.IsInvoiceWiseReturn = "0";
                    }

                    if (row[54].ToString() == "1")
                    {
                        rep1.IspopUpPDCheque = row[54].ToString();
                    }
                    else
                    {
                        rep1.IspopUpPDCheque = "0";
                    }



                    if (row[55].ToString() == "1")
                    {
                        rep1.showItemDetails = row[55].ToString();
                    }
                    else
                    {
                        rep1.showItemDetails = "0";
                    }

                    if (row[56].ToString() == "1")
                    {
                        rep1.IsMenuLandScape_rMST = row[56].ToString();
                    }
                    else
                    {
                        rep1.IsMenuLandScape_rMST = "0";
                    }

                    if (row[57].ToString() == "1")
                    {
                        rep1.IsReprint_rMST = row[57].ToString();
                    }
                    else
                    {
                        rep1.IsReprint_rMST = "0";
                    }

                    if (row[58].ToString() == "1")
                    {
                        rep1.LineFreeIssue_rMST = row[58].ToString();
                    }
                    else
                    {
                        if (row[58].ToString() == "100")
                        {
                            rep1.LineFreeIssue_rMST = row[58].ToString();
                        }
                        else
                        {
                            rep1.LineFreeIssue_rMST = "0";
                        }
                    }

                    if (row[59].ToString() == "1")
                    {
                        rep1.ISPrinterL50_rMST = row[59].ToString();
                    }
                    else
                    {
                        rep1.ISPrinterL50_rMST = "0";
                    }

                    if (row[60].ToString() == "1")
                    {
                        rep1.IsitemLoadCatwise_rMST = row[60].ToString();
                    }
                    else
                    {
                        rep1.IsitemLoadCatwise_rMST = "0";
                    }

                    if (row[61].ToString() == "1")
                    {
                        rep1.IsKeepItemCatolog_rMST = row[61].ToString();
                    }
                    else
                    {
                        rep1.IsKeepItemCatolog_rMST = "0";
                    }


                    if (row[65].ToString() != "")
                    {
                        rep1.BusinessUnitName = row[65].ToString();
                    }
                    else
                    {
                        rep1.BusinessUnitName = "0";
                    }


                    //----------------------------------------------------------- 2019-05-27

                    if (row[62].ToString() == "1")
                    {
                        rep1.keepPotasialItem = row[62].ToString();
                    }
                    else
                    {
                        rep1.keepPotasialItem = "0";
                    }

                    if (row[63].ToString() == "1")
                    {
                        rep1.ISCheckOnlineQty = row[63].ToString();
                    }
                    else
                    {
                        rep1.ISCheckOnlineQty = "0";
                    }
                    if (row[63].ToString() == "1")
                    {
                        rep1.ISShowLossOrders = row[63].ToString();
                    }
                    else
                    {
                        rep1.ISShowLossOrders = "0";

                    }


                    if (row[66].ToString() == "1")
                    {
                        rep1.SkiprouteP = row[66].ToString();
                    }
                    else
                    {
                        rep1.SkiprouteP = "0";

                    }


                    if (row[67].ToString() == "Old")
                    {
                        rep1.freeIssueType = row[67].ToString();
                    }
                    else
                    {
                        rep1.freeIssueType = "New";

                    }

                    if (row["ShopAuditNum"].ToString() != "")
                    {
                        rep1.ShopAuditNum = row["ShopAuditNum"].ToString();
                    }
                    else
                    {
                        rep1.ShopAuditNum = "0";

                    }



                    //---------------------------------------------------------------------------

                    if (row["MacIdQuo"].ToString() != "")
                    {
                        rep1.MacIdQuo = row["MacIdQuo"].ToString();
                    }
                    else
                    {
                        rep1.MacIdQuo = "0";
                    }

                    if (row["IsEnableQuotation"].ToString() != "")
                    {
                        rep1.IsEnableQuotation = row["IsEnableQuotation"].ToString();
                    }
                    else
                    {
                        rep1.IsEnableQuotation = "0";
                    }

                    if (row["IsEnableShopAudit"].ToString() != "")
                    {
                        rep1.IsEnableShopAudit = row["IsEnableShopAudit"].ToString();
                    }
                    else
                    {
                        rep1.IsEnableShopAudit = "0";
                    }

                    if (row["IsEnableBarcodeHeader"].ToString() != "")
                    {
                        rep1.IsEnableBarcodeHeader = row["IsEnableBarcodeHeader"].ToString();
                    }
                    else
                    {
                        rep1.IsEnableBarcodeHeader = "0";
                    }

                    if (row["IsEnableBarcodeLine"].ToString() != "")
                    {
                        rep1.IsEnableBarcodeLine = row["IsEnableBarcodeLine"].ToString();
                    }
                    else
                    {
                        rep1.IsEnableBarcodeLine = "0";
                    }

                    if (row["IsConvertToInvoice"].ToString() != "")
                    {
                        rep1.IsConvertToInvoice = row["IsConvertToInvoice"].ToString();
                    }
                    else
                    {
                        rep1.IsConvertToInvoice = "0";
                    }


                    if (row["IsShowItemImage"].ToString() != "")
                    {
                        rep1.IsShowItemImage = row["IsShowItemImage"].ToString();
                    }
                    else
                    {
                        rep1.IsShowItemImage = "0";
                    }
                    if (row["IsEnableSalesReturn"].ToString() != "")
                    {
                        rep1.IsEnableSalesReturn = row["IsEnableSalesReturn"].ToString();
                    }
                    else
                    {
                        rep1.IsEnableSalesReturn = "0";
                    }
                    if (row["IsEnableMarketReturn"].ToString() != "")
                    {
                        rep1.IsEnableMarketReturn = row["IsEnableMarketReturn"].ToString();
                    }
                    else
                    {
                        rep1.IsEnableMarketReturn = "0";
                    }
                    if (row["IsEnableBulkReceipt"].ToString() != "")
                    {
                        rep1.IsEnableBulkReceipt = row["IsEnableBulkReceipt"].ToString();
                    }
                    else
                    {
                        rep1.IsEnableBulkReceipt = "0";
                    }
                    if (row["IsEnableSaleshistory"].ToString() != "")
                    {
                        rep1.IsEnableSaleshistory = row["IsEnableSaleshistory"].ToString();
                    }
                    else
                    {
                        rep1.IsEnableSaleshistory = "0";
                    }
                    if (row["IsEnableReceiptHistory"].ToString() != "")
                    {
                        rep1.IsEnableReceiptHistory = row["IsEnableReceiptHistory"].ToString();
                    }
                    else
                    {
                        rep1.IsEnableReceiptHistory = "0";
                    }
                    if (row["IsEnableChangePriceForSale"].ToString() != "")
                    {
                        rep1.IsEnableChangePriceForSale = row["IsEnableChangePriceForSale"].ToString();
                    }
                    else
                    {
                        rep1.IsEnableChangePriceForSale = "0";
                    }
                    if (row["IsEnableShowItemBatch"].ToString() != "")
                    {
                        rep1.IsEnableShowItemBatch = row["IsEnableShowItemBatch"].ToString();
                    }
                    else
                    {
                        rep1.IsEnableShowItemBatch = "0";
                    }
                    listrep.Add(rep1);


                }
            }
            else
            {
                rep1.RepKey = "0";
                rep1.RepCode = "INVALID";
                rep1.RepName = "rep1Code Not Exists";
            }

            sqlCommand.Dispose();
            sqlConn.Close();

            return listrep.ToArray();
        }


        public BusinessUnit[] getJsonBusinessUnit(string loginName, string DID)
        {
            var list = new List<BusinessUnit>();

            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "dbo.Mobile_SalesRep";

            sqlCommand.Parameters.Add(new SqlParameter("@Sel", "2"));
            sqlCommand.Parameters.Add(new SqlParameter("@CompanyID", "1"));
            sqlCommand.Parameters.Add(new SqlParameter("@LoginName", loginName));
            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                BusinessUnit bunit = new BusinessUnit();
                bunit.CompID = row[0].ToString();
                bunit.BusinessUnitName = row[1].ToString();
                list.Add(bunit);
            }

            sqlCommand.Dispose();
            sqlConn.Close();

            return list.ToArray();
        }

        public DownloadRecSummary getJsonDownloadRecSummary(string repID, string comp)
        {
            var sum = new DownloadRecSummary();

            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "dbo.Mobile_NumOfRecordSelect";

            sqlCommand.Parameters.Add(new SqlParameter("@Sel", "1"));
            sqlCommand.Parameters.Add(new SqlParameter("@CompanyID", comp));
            sqlCommand.Parameters.Add(new SqlParameter("@RepID", repID));
            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    sum.NumOfCustomers = row[0].ToString();
                    sum.NumOfProducts = row[1].ToString();
                    sum.NumOfPriceListsPrices = row[2].ToString();
                }
            }
            else
            {
                sum.NumOfCustomers = "0";
                sum.NumOfProducts = "0";
                sum.NumOfPriceListsPrices = "0";
            }

            sqlCommand.Dispose();
            sqlConn.Close();

            return sum;
        }

        public InvoiceLineDisplay[] getJsonInvoiceLineDisplay(string comp, string inv)
        {
            var list = new List<InvoiceLineDisplay>();

            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "dbo.Mobile_InvoiceHeaderelect";

            sqlCommand.Parameters.Add(new SqlParameter("@Sel", "1"));
            sqlCommand.Parameters.Add(new SqlParameter("@CompanyID", comp));
            sqlCommand.Parameters.Add(new SqlParameter("@InvoiceCode", inv));
            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                var invDis = new InvoiceLineDisplay
                {
                    CustomerCode = row["Account"].ToString(),
                    CustomerName = row["Name"].ToString(),
                    InvoiceCode =
                        row["InvNumber"].ToString().Length > 1
                            ? row["InvNumber"].ToString()
                            : row["OrderNum"].ToString(),
                    InvoiceDate = row["InvDate"].ToString(),
                    HeaderGross = row["InvTotInclDEx"].ToString(),
                    HeaderDiscPerc = row["InvDisc"].ToString(),
                    HeaderNett = row["InvTotIncl"].ToString(),
                    ProductCode = row["cDescription"].ToString(),
                    ProductDescription = row["cDescription"].ToString(),
                    Qty = row["fQtyProcessed"].ToString(),
                    InclPrice = row["fUnitPriceIncl"].ToString(),
                    LineDiscPerc = row["fLineDiscount"].ToString(),
                    LineValue = row["fQuantityLineTotIncl"].ToString()
                };
                list.Add(invDis);
            }

            sqlCommand.Dispose();
            sqlConn.Close();

            return list.ToArray();
        }

        public string SaveSalesReturn(RequestData rdata)
        {
            SqlCommand sqlCommand = new SqlCommand();
            SqlConnection sqlConn = new SqlConnection();
            string strResult = "ok";
            //string strPath = HostingEnvironment.MapPath("log.txt");
            string RecFlag = "1";


            try
            {
                //sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
                //sqlCommand.CommandType = CommandType.StoredProcedure;
                //sqlCommand.CommandText = "dbo.MobileInvoiceLine_InsertUpdate";

                //// input param

                //sqlCommand.Parameters.Add(new SqlParameter("@DeviceID", invd.DeviceID));
                //sqlCommand.Parameters.Add(new SqlParameter("@CompID", invd.CompID));
                //sqlCommand.Parameters.Add(new SqlParameter("@AutoID", invd.AutoID));
                //sqlCommand.Parameters.Add(new SqlParameter("@InvHeaderID", invd.InvHeaderID));
                //sqlCommand.Parameters.Add(new SqlParameter("@InvoiceCode", invd.InvoiceCode));
                //sqlCommand.Parameters.Add(new SqlParameter("@StockLink", invd.StockLink));
                //sqlCommand.Parameters.Add(new SqlParameter("@Qty", invd.Qty));
                //sqlCommand.Parameters.Add(new SqlParameter("@ProcessedQty", invd.ProcessedQty));
                //sqlCommand.Parameters.Add(new SqlParameter("@UnitExclPrice", invd.UnitExclPrice));
                //sqlCommand.Parameters.Add(new SqlParameter("@UnitInclPrice", invd.UnitInclPrice));
                //sqlCommand.Parameters.Add(new SqlParameter("@LineDiscPerc", invd.LineDiscPerc));
                //sqlCommand.Parameters.Add(new SqlParameter("@LineTaxAmount", invd.LineTaxAmount));
                //sqlCommand.Parameters.Add(new SqlParameter("@LineExclNoDisc", invd.LineExclNoDisc));
                //sqlCommand.Parameters.Add(new SqlParameter("@LineInclNoDisc", invd.LineInclNoDisc));
                //sqlCommand.Parameters.Add(new SqlParameter("@LineExclWithDisc", invd.LineExclWithDisc));
                //sqlCommand.Parameters.Add(new SqlParameter("@LineInclWithDisc", invd.LineInclWithDisc));
                //sqlCommand.Parameters.Add(new SqlParameter("@SalesType", invd.SalesType));
                //sqlCommand.Parameters.Add(new SqlParameter("@RecFlag", RecFlag));

                //SqlParameter returnString = sqlCommand.Parameters.Add("@RetID", SqlDbType.Int);
                //returnString.Direction = ParameterDirection.Output;

                //sqlConn.Open();

                //sqlCommand.Connection = sqlConn;
                //int intResult = sqlCommand.ExecuteNonQuery();
                //strResult = returnString.Value.ToString();
                //strResult = "Ok";
                //  file.WriteLine("strResult");
                strResult = "Saved: Return INVOICE: " + rdata.InvoiceCode;
            }
            catch (Exception ex)
            {
                strResult = "SaveSalesReturn: " + ex.Message;
                //  file.WriteLine(strResult);

            }
            // file.Close();
            return strResult;
        }
        public string UploadReceiptHeader2(ReceiptHeader rech)
        {
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            string strResult = "OK";
            string strResultLine = "OK";
            //string strPath = HostingEnvironment.MapPath("log.txt");
            int Numberofline = 0;
            int actualLineCount = 0;

            try
            {
                sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "dbo.Mobile_ReceiptHeaderInsertUpdate";

                // input param
                sqlCommand.Parameters.Add(new SqlParameter("@DeviceID", rech.DeviceID));
                sqlCommand.Parameters.Add(new SqlParameter("@CompID", rech.CompID));
                sqlCommand.Parameters.Add(new SqlParameter("@AutoID", rech.AutoID));
                sqlCommand.Parameters.Add(new SqlParameter("@RecID", rech.RecID));
                sqlCommand.Parameters.Add(new SqlParameter("@ReceiptCode", rech.ReceiptCode));
                sqlCommand.Parameters.Add(new SqlParameter("@ReceiptDate", rech.ReceiptDate));
                sqlCommand.Parameters.Add(new SqlParameter("@AccountID", rech.AccountID));
                sqlCommand.Parameters.Add(new SqlParameter("@CustomerCode", rech.CustomerCode));
                sqlCommand.Parameters.Add(new SqlParameter("@CustomerName", rech.AccountID));
                sqlCommand.Parameters.Add(new SqlParameter("@PaymentMethod", rech.PaymentMethod));
                sqlCommand.Parameters.Add(new SqlParameter("@ChequeNo", rech.ChequeNo));
                sqlCommand.Parameters.Add(new SqlParameter("@ChequeDate", rech.ChequeDate));
                sqlCommand.Parameters.Add(new SqlParameter("@BankBranch", rech.BankBranch));
                sqlCommand.Parameters.Add(new SqlParameter("@Amount", rech.Amount));
                sqlCommand.Parameters.Add(new SqlParameter("@NumberOfLine", rech.NumberOfLine));
                sqlCommand.Parameters.Add(new SqlParameter("@RecFlag", rech.RecFlag));
                sqlCommand.Parameters.Add(new SqlParameter("@RepID", rech.RepID));
                sqlCommand.Parameters.Add(new SqlParameter("@isReturncheque", rech.isReturncheque));
                var returnString = sqlCommand.Parameters.Add("@RetID", SqlDbType.Int);
                returnString.Direction = ParameterDirection.Output;

                sqlConn.Open();

                sqlCommand.Connection = sqlConn;
                var intResult = sqlCommand.ExecuteNonQuery();
                strResult = returnString.Value.ToString();
                sqlConn.Close();
                //strResult = "200: Ok";
                //file.WriteLine("strResult");
                if (Convert.ToInt32(strResult) != -1)
                {
                    foreach (ReceiptLine recd in rech.recd)
                    {

                        try
                        {

                            actualLineCount = actualLineCount + 1;

                            sqlCommand = new SqlCommand();
                            sqlConn = new SqlConnection();
                            var RecFlag = "1";

                            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
                            sqlCommand.CommandType = CommandType.StoredProcedure;
                            sqlCommand.CommandText = "dbo.Mobile_ReceiptLineInsertUpdate";

                            // input param

                            sqlCommand.Parameters.Add(new SqlParameter("@DeviceID", recd.DeviceID));
                            sqlCommand.Parameters.Add(new SqlParameter("@CompID", recd.CompID));
                            sqlCommand.Parameters.Add(new SqlParameter("@AutoID", recd.AutoID));
                            sqlCommand.Parameters.Add(new SqlParameter("@RecID", recd.RecID));
                            sqlCommand.Parameters.Add(new SqlParameter("@RepID", recd.fRepID));
                            sqlCommand.Parameters.Add(new SqlParameter("@ReceiptHeaderID", recd.ReceiptHeaderID));
                            sqlCommand.Parameters.Add(new SqlParameter("@ReceiptCode", recd.ReceiptCode));
                            sqlCommand.Parameters.Add(new SqlParameter("@InvoiceCode", recd.InvoiceCode));
                            sqlCommand.Parameters.Add(new SqlParameter("@InvoiceDate", recd.InvoiceDate));
                            sqlCommand.Parameters.Add(new SqlParameter("@InvoiceAmount", recd.InvoiceAmount));
                            sqlCommand.Parameters.Add(new SqlParameter("@SetOffAmount", recd.SetOffAmount));
                            sqlCommand.Parameters.Add(new SqlParameter("@RecFlag", recd.RecFlag));
                            var returnStringLine = sqlCommand.Parameters.Add("@RetID", SqlDbType.Int);
                            returnStringLine.Direction = ParameterDirection.Output;



                            sqlConn.Open();

                            sqlCommand.Connection = sqlConn;
                            var intResultLine = sqlCommand.ExecuteNonQuery();
                            strResultLine = returnStringLine.Value.ToString();
                            strResultLine = returnStringLine.Value.ToString();
                            sqlConn.Close();


                            if (Convert.ToInt32(strResultLine) != -1)
                            {

                            }
                            else
                            {
                                strResult = "UploadReceiptHeader: -1";
                                break;

                                //return "UploadInvoiceHeader: -1";
                            }

                        }
                        catch (Exception ex)
                        {
                            strResultLine = "Line insert: " + ex.Message;
                            strResult = strResultLine;
                            break;
                            //  file.WriteLine(strResult);

                        }
                    }



                    //if (CstrResultLine)
                    //  file.WriteLine("strResult");


                }

            }

            catch (Exception ex)
            {
                strResult = "UploadReceiptHeader: " + ex.Message;
                //  file.WriteLine(strResult);

            }
            // file.Close();
            return strResult;
        }
        public string UploadReceiptHeader(ReceiptHeader rech)
        {
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var strResult = "ok";
            //string strPath = HostingEnvironment.MapPath("log.txt");

            try
            {
                sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "dbo.Mobile_ReceiptHeaderInsertUpdate";

                // input param
                sqlCommand.Parameters.Add(new SqlParameter("@DeviceID", rech.DeviceID));
                sqlCommand.Parameters.Add(new SqlParameter("@CompID", rech.CompID));
                sqlCommand.Parameters.Add(new SqlParameter("@AutoID", rech.AutoID));
                sqlCommand.Parameters.Add(new SqlParameter("@RecID", rech.RecID));
                sqlCommand.Parameters.Add(new SqlParameter("@ReceiptCode", rech.ReceiptCode));
                sqlCommand.Parameters.Add(new SqlParameter("@ReceiptDate", rech.ReceiptDate));
                sqlCommand.Parameters.Add(new SqlParameter("@AccountID", rech.AccountID));
                sqlCommand.Parameters.Add(new SqlParameter("@CustomerCode", rech.CustomerCode));
                sqlCommand.Parameters.Add(new SqlParameter("@CustomerName", rech.AccountID));
                sqlCommand.Parameters.Add(new SqlParameter("@PaymentMethod", rech.PaymentMethod));
                sqlCommand.Parameters.Add(new SqlParameter("@ChequeNo", rech.ChequeNo));
                sqlCommand.Parameters.Add(new SqlParameter("@ChequeDate", rech.ChequeDate));
                sqlCommand.Parameters.Add(new SqlParameter("@BankBranch", rech.BankBranch));
                sqlCommand.Parameters.Add(new SqlParameter("@Amount", rech.Amount));
                sqlCommand.Parameters.Add(new SqlParameter("@NumberOfLine", rech.NumberOfLine));
                sqlCommand.Parameters.Add(new SqlParameter("@RecFlag", rech.RecFlag));
                sqlCommand.Parameters.Add(new SqlParameter("@RepID", rech.RepID));
                sqlCommand.Parameters.Add(new SqlParameter("@isReturncheque", rech.isReturncheque));
                var returnString = sqlCommand.Parameters.Add("@RetID", SqlDbType.Int);
                returnString.Direction = ParameterDirection.Output;

                sqlConn.Open();

                sqlCommand.Connection = sqlConn;
                var intResult = sqlCommand.ExecuteNonQuery();
                strResult = returnString.Value.ToString();
                sqlConn.Close();
                //strResult = "200: Ok";
                //file.WriteLine("strResult");
            }
            catch (Exception ex)
            {
                strResult = "UploadReceiptHeader: " + ex.Message;
                //  file.WriteLine(strResult);

            }
            // file.Close();
            return strResult;
        }

        public string UploadReceiptLine(ReceiptLine recd)
        {
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var strResult = "ok";
            //string strPath = HostingEnvironment.MapPath("log.txt");
            string RecFlag = "1";
            int intResult;
            int i = 0;

            try
            {

                sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "dbo.Mobile_ReceiptLineInsertUpdate";
                i = i + 1;//1
                // input param
                sqlCommand.Parameters.Add(new SqlParameter("@DeviceID", recd.DeviceID));
                sqlCommand.Parameters.Add(new SqlParameter("@CompID", recd.CompID));
                sqlCommand.Parameters.Add(new SqlParameter("@AutoID", recd.AutoID));
                sqlCommand.Parameters.Add(new SqlParameter("@RecID", recd.RecID));
                sqlCommand.Parameters.Add(new SqlParameter("@RepID", recd.RepID));
                sqlCommand.Parameters.Add(new SqlParameter("@ReceiptHeaderID", recd.ReceiptHeaderID));
                sqlCommand.Parameters.Add(new SqlParameter("@ReceiptCode", recd.ReceiptCode));
                sqlCommand.Parameters.Add(new SqlParameter("@InvoiceCode", recd.InvoiceCode));
                sqlCommand.Parameters.Add(new SqlParameter("@InvoiceDate", recd.InvoiceDate));
                sqlCommand.Parameters.Add(new SqlParameter("@InvoiceAmount", recd.InvoiceAmount));
                sqlCommand.Parameters.Add(new SqlParameter("@SetOffAmount", recd.SetOffAmount));
                sqlCommand.Parameters.Add(new SqlParameter("@RecFlag", recd.RecFlag));
                var returnString = sqlCommand.Parameters.Add("@RetID", SqlDbType.Int);
                returnString.Direction = ParameterDirection.Output;
                sqlConn.Open();
                sqlCommand.Connection = sqlConn;
                intResult = sqlCommand.ExecuteNonQuery();
                strResult = returnString.Value.ToString();
                strResult = "200: Ok";
                sqlConn.Close();
                //file.WriteLine("strResult");
            }
            catch (Exception ex)
            {
                strResult = "UploadReceiptDetail: " + i.ToString() + ": " + ex.Message;
                //  file.WriteLine(strResult);

            }
            // file.Close();
            return strResult;
        }

        public FreeIssuePolicy[] getJsonFreeIssuePolicy(string comp)
        {
            var list = new List<FreeIssuePolicy>();
            // var strSpilt = comp.Split('~');       




            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "dbo.Mobile_FreeIssuePolicySelect";

            sqlCommand.Parameters.Add(new SqlParameter("@Sel", "1"));
            sqlCommand.Parameters.Add(new SqlParameter("@CompanyID", comp));

            //if (strSpilt.Length==2)
            //    sqlCommand.Parameters.Add(new SqlParameter("@Selection1", int.Parse(strSpilt[1])));
            //else
            sqlCommand.Parameters.Add(new SqlParameter("@Selection1", "100"));
            sqlCommand.Parameters.Add(new SqlParameter("@Selection2", "10"));
            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);






            foreach (DataRow row in dt.Rows)
            {
                var fiPolicy = new FreeIssuePolicy
                {
                    AutoID = row[0].ToString(),
                    ScheemCode = row[1].ToString(),
                    Description = row[2].ToString(),
                    ScheemType = row[3].ToString(),
                    ItemLink = row[4].ToString(),
                    ItemCode = row[5].ToString(),
                    ItemDescription = row[6].ToString(),
                    Quantity = row[7].ToString(),
                    ItemLinkFI = row[8].ToString(),
                    ItemCodeFI = row[9].ToString(),
                    ItemDescriptionFI = row[10].ToString(),
                    FreeIssueQuantity = row[11].ToString(),
                    SelectionType = row[12].ToString(),
                    PaymentMethod = row["PaymentMethod"].ToString(),
                    ItemGroupLink = row["ItemGroupLink"].ToString(),
                    ItemGroup = row["ItemGroup"].ToString(),
                    DCLink = row["DCLink"].ToString(),
                    classID = row["IdCliClass"].ToString()
                };
                list.Add(fiPolicy);
            }

            sqlCommand.Dispose();
            sqlConn.Close();

            return list.ToArray();
        }

        public SalesCustomerProducts[] getJsonSalesCustomerProducts(string Sel, string CompID, string CusID, string RepID, string StockID, string FromDate, string ToDate)
        {
            var list = new List<SalesCustomerProducts>();
            //string FromDate = "06/01/2015";
            //string ToDate = "06/31/2015";
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "Mobile_DashboardDataSelect";

            sqlCommand.Parameters.Add(new SqlParameter("@Sel", Sel));
            sqlCommand.Parameters.Add(new SqlParameter("@CompID", CompID));
            sqlCommand.Parameters.Add(new SqlParameter("@CusID", CusID));
            sqlCommand.Parameters.Add(new SqlParameter("@RepID", RepID));
            sqlCommand.Parameters.Add(new SqlParameter("@StockID", StockID));
            sqlCommand.Parameters.Add(new SqlParameter("@FromDate", FromDate));
            sqlCommand.Parameters.Add(new SqlParameter("@ToDate", ToDate));
            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                var sales = new SalesCustomerProducts
                {
                    CustomerCode = row[0].ToString(),
                    CustomerName = row[1].ToString(),
                    ProductGroup = row[2].ToString(),
                    ProductCode = row[3].ToString(),
                    ProductDescription = row[4].ToString(),
                    QTY = row[5].ToString(),
                    VALUE = row[6].ToString()
                };
                list.Add(sales);
            }

            sqlCommand.Dispose();
            sqlConn.Close();

            return list.ToArray();
        }

        public RepTarget[] GetJsonRepTarget(string CompID, string RepID, string Year, string Month)
        {
            //string FromDate = "06/01/2015";
            //string ToDate = "06/31/2015";
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            var year = DateTime.Now.Year;
            var month = DateTime.Now.Month;

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "dbo.Mobile_RepTarget";


            sqlCommand.Parameters.Add(new SqlParameter("@CompanyID", int.Parse(CompID)));
            sqlCommand.Parameters.Add(new SqlParameter("@RepID", int.Parse(RepID)));
            sqlCommand.Parameters.Add(new SqlParameter("@Year", Year));
            sqlCommand.Parameters.Add(new SqlParameter("@Month", Month));

            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            sqlCommand.Dispose();
            sqlConn.Close();


            IFormatProvider culture = new CultureInfo("en-US", true);

            return (from DataRow row in dt.Rows
                    select new RepTarget
                    {
                        AccountNo = row[9].ToString(),
                        Qty = row[13].ToString(),
                        StockLink = row[18].ToString(),
                        Target = row["TotalTarget"].ToString(),
                        RecYear = row["RecYear"].ToString(),
                        RecMonth = row["RecMonth"].ToString(),
                        ///trgt_date = DateTime.ParseExact(row["trgt_date"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture).ToShortDateString()
                        //trgt_date = DateTime.ParseExact(row["trgt_date"].ToString(), "yyyy-MM-dd", culture).ToString()
                        RepCode = row["RepCode"].ToString(), // catogory 
                        ItemCode = row["ItemCode"].ToString(), // sub catogry
                        trgt_date = row["trgt_date"].ToString()


                    }).ToArray();
        }

        //public string getJsonSalesCustomerProducts(string Sel, string CompID, string CusID, string RepID, string StockID, string FromDate, string ToDate)
        //{

        //    return FromDate + " and " + ToDate;
        //}


        public Tax[] getJsonTaxRate()
        {
            var list = new List<Tax>();

            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "Mobile_Tax";
            sqlCommand.Parameters.Add(new SqlParameter("@Sel", 1));

            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                var tax = new Tax();
                tax.Code = row[0].ToString();
                tax.Description = row[1].ToString();
                tax.TaxRate = row[2].ToString();
                tax.TaxRate_iBranchID = row[3].ToString();
                list.Add(tax);
            }

            sqlCommand.Dispose();
            sqlConn.Close();

            return list.ToArray();
        }




        public string InsertSpecialDiscount(SpecialDiscount special)
        {
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var strResult = "ok";
            //string strPath = HostingEnvironment.MapPath("log.txt");
            var RecFlag = "1";


            try
            {

                sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "InsertSpecialDiscount";

                // input param

                sqlCommand.Parameters.Add(new SqlParameter("@invoiceNo", special.invoiceNo));
                sqlCommand.Parameters.Add(new SqlParameter("@itemGroup", special.itemGroup));
                sqlCommand.Parameters.Add(new SqlParameter("@itemGroupLink", special.itemGroupLink));
                sqlCommand.Parameters.Add(new SqlParameter("@total", float.Parse(special.total)));
                sqlCommand.Parameters.Add(new SqlParameter("@dRate", float.Parse(special.dRate)));

                sqlConn.Open();

                sqlCommand.Connection = sqlConn;
                var intResult = sqlCommand.ExecuteNonQuery();
                // strResult = returnString.Value.ToString();
                strResult = "Ok";
                sqlConn.Close();
                //  file.WriteLine("strResult");
            }
            catch (Exception ex)
            {
                strResult = "Error ";
                //  file.WriteLine(strResult);

            }
            // file.Close();
            return strResult;
        }



        public string InsertLot(Lot lot)
        {
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var strResult = "ok";
            //string strPath = HostingEnvironment.MapPath("log.txt");
            var RecFlag = "1";


            try
            {

                sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "Mobile_InsertSalesLot";

                // input param

                sqlCommand.Parameters.Add(new SqlParameter("@InvoiceNo", lot.InvoiceNo));
                sqlCommand.Parameters.Add(new SqlParameter("@StockLink", int.Parse(lot.StockLink)));
                sqlCommand.Parameters.Add(new SqlParameter("@LotID", lot.LotID));
                sqlCommand.Parameters.Add(new SqlParameter("@Qty", float.Parse(lot.Qty)));
                sqlCommand.Parameters.Add(new SqlParameter("@ItemStatus", lot.ItemStatus));

                sqlConn.Open();

                sqlCommand.Connection = sqlConn;
                var intResult = sqlCommand.ExecuteNonQuery();
                // strResult = returnString.Value.ToString();
                strResult = "Ok";
                sqlConn.Close();
                //  file.WriteLine("strResult");
            }
            catch (Exception ex)
            {
                strResult = "Error " + ex.ToString();
                //  file.WriteLine(strResult);

            }
            // file.Close();
            return strResult;
        }

        public MobileCancelInv[] getJsonCInvResult(string CompID, string RepID)
        {
            try
            {
                var sqlCommand = new SqlCommand();
                var sqlConn = new SqlConnection();
                var dt = new DataTable();

                sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "Mobile_CancelInvoice";
                sqlCommand.Parameters.Add(new SqlParameter("@Sel", 1));
                sqlCommand.Parameters.Add(new SqlParameter("@RepID", int.Parse(RepID)));
                sqlCommand.Parameters.Add(new SqlParameter("@CompID", int.Parse(CompID)));
                sqlConn.Open();
                sqlCommand.Connection = sqlConn;
                var da = new SqlDataAdapter(sqlCommand);
                da.Fill(dt);

                sqlCommand.Dispose();
                sqlConn.Close();

                return (from DataRow row in dt.Rows
                        select new MobileCancelInv
                        {
                            invoiceCode = row[0].ToString(),
                            SalesType = row[1].ToString(),
                            Status = row[2].ToString()

                        }).ToArray();
            }
            catch (Exception ex)
            {
                return null;
                //  throw;
            }

        }


        public ReturnCheque[] getJsonReturnCheque(string CompID, string RepID)
        {
            try
            {
                var sqlCommand = new SqlCommand();
                var sqlConn = new SqlConnection();
                var dt = new DataTable();

                sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "Mobile_ReturnCheque";


                sqlCommand.Parameters.Add(new SqlParameter("@Sel", 1));
                sqlCommand.Parameters.Add(new SqlParameter("@RepID", int.Parse(RepID)));
                sqlCommand.Parameters.Add(new SqlParameter("@CompID", int.Parse(CompID)));
                sqlConn.Open();
                sqlCommand.Connection = sqlConn;
                var da = new SqlDataAdapter(sqlCommand);
                da.Fill(dt);

                sqlCommand.Dispose();
                sqlConn.Close();

                return (from DataRow row in dt.Rows
                        select new ReturnCheque
                        {
                            AccountId = row[0].ToString(),
                            BankID = row[1].ToString(),
                            ReturnChequeNumber = row[2].ToString(),
                            RecieptCode = row[3].ToString(),

                        }).ToArray();
            }
            catch (Exception ex)
            {
                return null;
                //  throw;
            }

        }


        public BankMaster[] getJsonBankMaster(string CompID, string RepID)
        {
            try
            {
                var sqlCommand = new SqlCommand();
                var sqlConn = new SqlConnection();
                var dt = new DataTable();

                sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "Mobile_BankMaster";


                sqlCommand.Parameters.Add(new SqlParameter("@Sel", 1));
                sqlCommand.Parameters.Add(new SqlParameter("@RepID", int.Parse(RepID)));
                sqlCommand.Parameters.Add(new SqlParameter("@DistributorID ", int.Parse(CompID)));
                sqlConn.Open();
                sqlCommand.Connection = sqlConn;
                var da = new SqlDataAdapter(sqlCommand);
                da.Fill(dt);

                sqlCommand.Dispose();
                sqlConn.Close();

                return (from DataRow row in dt.Rows
                        select new BankMaster
                        {
                            BankID = row[0].ToString(),
                            BankCode = row[1].ToString(),
                            BankName = row[2].ToString()

                        }).ToArray();
            }
            catch (Exception ex)
            {
                return null;
                //  throw;
            }

        }




        public string UpdateCancelInvResponse(CancelInvResponse CInvResponse)
        {
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var strResult = "ok";
            //string strPath = HostingEnvironment.MapPath("log.txt");
            var RecFlag = "1";


            try
            {

                sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "Mobile_CancelInvoice_Response";

                // input param

                sqlCommand.Parameters.Add(new SqlParameter("@RepID", int.Parse(CInvResponse.repId)));
                sqlCommand.Parameters.Add(new SqlParameter("@CompID", int.Parse(CInvResponse.compId)));
                sqlCommand.Parameters.Add(new SqlParameter("@InvoiceCode", CInvResponse.invoiceNo));


                sqlConn.Open();

                sqlCommand.Connection = sqlConn;
                var intResult = sqlCommand.ExecuteNonQuery();
                // strResult = returnString.Value.ToString();
                strResult = "Ok";
                sqlConn.Close();
                //  file.WriteLine("strResult");
            }
            catch (Exception ex)
            {
                strResult = "Error ";
                //  file.WriteLine(strResult);

            }
            // file.Close();
            return strResult;
        }

        public string InsertJsonBanking(BankDeposit bpDeposit)
        {
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var strResult = "ok";
            //string strPath = HostingEnvironment.MapPath("log.txt");
            var RecFlag = "1";


            try
            {

                sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "Mobile_Bank_Deposit";

                // input param

                sqlCommand.Parameters.Add(new SqlParameter("@BankRecipetNo", bpDeposit.recieptNo));
                sqlCommand.Parameters.Add(new SqlParameter("@Amount", bpDeposit.amount));
                sqlCommand.Parameters.Add(new SqlParameter("@Comment", bpDeposit.comment));
                sqlCommand.Parameters.Add(new SqlParameter("@Type", bpDeposit.type));
                sqlCommand.Parameters.Add(new SqlParameter("@DepositDate", bpDeposit.date));
                sqlCommand.Parameters.Add(new SqlParameter("@RepID", bpDeposit.RepID));
                sqlCommand.Parameters.Add(new SqlParameter("@CompID", bpDeposit.ComID));
                sqlCommand.Parameters.Add(new SqlParameter("@TabId", bpDeposit.TabID));
                sqlConn.Open();

                sqlCommand.Connection = sqlConn;
                var intResult = sqlCommand.ExecuteNonQuery();
                // strResult = returnString.Value.ToString();
                strResult = "Ok";
                sqlConn.Close();
                //  file.WriteLine("strResult");
            }
            catch (Exception ex)
            {
                // strResult = "Error ";
                strResult = ex.Message;
                //  file.WriteLine(strResult);

            }
            // file.Close();
            return strResult;
        }

        public string InsertJsonCustomerVisit(CustomerVisit CVisit)
        {
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var strResult = "ok";
            //string strPath = HostingEnvironment.MapPath("log.txt");
            var RecFlag = "1";


            try
            {

                sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "Mobile_InsertCustomerVisit";

                // input param

                sqlCommand.Parameters.Add(new SqlParameter("@repId", CVisit.repId));
                sqlCommand.Parameters.Add(new SqlParameter("@compId", CVisit.compId));
                sqlCommand.Parameters.Add(new SqlParameter("@date", CVisit.date));
                sqlCommand.Parameters.Add(new SqlParameter("dcLink", CVisit.dcLink));
                sqlCommand.Parameters.Add(new SqlParameter("@eDateTime", CVisit.sDateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@sDateTim", CVisit.sDateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@comment", CVisit.comment));
                sqlCommand.Parameters.Add(new SqlParameter("@customerCode", CVisit.customerCode));






                sqlConn.Open();

                sqlCommand.Connection = sqlConn;
                var intResult = sqlCommand.ExecuteNonQuery();
                // strResult = returnString.Value.ToString();
                strResult = "Ok";
                sqlConn.Close();
                //  file.WriteLine("strResult");
            }
            catch (Exception ex)
            {
                // strResult = "Error ";
                strResult = ex.Message;
                //  file.WriteLine(strResult);

            }
            // file.Close();
            return strResult;
        }


        public string InsertJsonMeaterReading(MeterReading CVisit)
        {
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var strResult = "ok";
            //string strPath = HostingEnvironment.MapPath("log.txt");
            var RecFlag = "1";


            try
            {

                sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "Mobile_InsertRepMeterReading";

                // input param

                sqlCommand.Parameters.Add(new SqlParameter("@MeterReadingID", CVisit.MeterReadingID));
                sqlCommand.Parameters.Add(new SqlParameter("@RepID", CVisit.RepID));
                sqlCommand.Parameters.Add(new SqlParameter("@DID", CVisit.DID));
                sqlCommand.Parameters.Add(new SqlParameter("@MeterDate", CVisit.MeterDate));
                sqlCommand.Parameters.Add(new SqlParameter("@DayStart", CVisit.DayStart));
                sqlCommand.Parameters.Add(new SqlParameter("@DayEnd", CVisit.DayEnd));
                sqlCommand.Parameters.Add(new SqlParameter("@Additinalusege", CVisit.Additinalusege));

                sqlConn.Open();

                sqlCommand.Connection = sqlConn;
                var intResult = sqlCommand.ExecuteNonQuery();
                // strResult = returnString.Value.ToString();
                strResult = "Ok";
                sqlConn.Close();
                //  file.WriteLine("strResult");
            }
            catch (Exception ex)
            {
                // strResult = "Error ";
                strResult = ex.Message;
                //  file.WriteLine(strResult);

            }
            // file.Close();
            return strResult;
        }



        public string InsertJsonnsertShopAuditData(ShopAudit CVisit)
        {
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var strResult = "ok";
            //string strPath = HostingEnvironment.MapPath("log.txt");
            var RecFlag = "1";


            try
            {

                sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "Mobile_InsertShopAuditData";

                // input param

                sqlCommand.Parameters.Add(new SqlParameter("@ID", CVisit.ID));
                sqlCommand.Parameters.Add(new SqlParameter("@ShAuditNum", CVisit.ShAuditNum));
                sqlCommand.Parameters.Add(new SqlParameter("@AuditDate", CVisit.AuditDate));
                sqlCommand.Parameters.Add(new SqlParameter("@DID", CVisit.DID));
                sqlCommand.Parameters.Add(new SqlParameter("@Repid", CVisit.Repid));
                sqlCommand.Parameters.Add(new SqlParameter("@Repcode", CVisit.Repcode));
                sqlCommand.Parameters.Add(new SqlParameter("@CID", CVisit.CID));
                sqlCommand.Parameters.Add(new SqlParameter("@CCode", CVisit.CCode));
                sqlCommand.Parameters.Add(new SqlParameter("@CName", CVisit.CName));
                sqlCommand.Parameters.Add(new SqlParameter("@ItemID", CVisit.ItemID));
                sqlCommand.Parameters.Add(new SqlParameter("@ItemCode", CVisit.ItemCode));
                sqlCommand.Parameters.Add(new SqlParameter("@ItemName", CVisit.ItemName));
                sqlCommand.Parameters.Add(new SqlParameter("@CountedQty", CVisit.CountedQty));
                sqlCommand.Parameters.Add(new SqlParameter("@SisMAvgsale", CVisit.SisMAvgsale));
                sqlCommand.Parameters.Add(new SqlParameter("@LastmothsaleAqty", CVisit.LastmothsaleAqty));
                sqlCommand.Parameters.Add(new SqlParameter("@SgusstedQty", CVisit.SgusstedQty));
                sqlCommand.Parameters.Add(new SqlParameter("@ActualQty", CVisit.ActualQty));



                sqlConn.Open();

                sqlCommand.Connection = sqlConn;
                var intResult = sqlCommand.ExecuteNonQuery();
                // strResult = returnString.Value.ToString();
                strResult = "Ok";
                sqlConn.Close();
                //  file.WriteLine("strResult");
            }
            catch (Exception ex)
            {
                // strResult = "Error ";
                strResult = ex.Message;
                //  file.WriteLine(strResult);

            }
            // file.Close();
            return strResult;
        }



        public RootMaster[] getJsonRootMaster(string distributorId, string rep)
        {
            var list = new List<RootMaster>();

            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "Mobile_RootMaster";


            sqlCommand.Parameters.Add(new SqlParameter("@CompID", distributorId));
            sqlCommand.Parameters.Add(new SqlParameter("@RepID ", rep));

            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                string hh = "hhh";
                var rootMaster = new RootMaster
                {
                    routeID = row[0].ToString(),
                    distributorID = row[1].ToString(),
                    repID = row[2].ToString(),
                    routeCode = row[3].ToString(),
                    routeDescription = row[4].ToString(),
                    parentRouteID = row[5].ToString(),
                    recFlag = row[6].ToString(),
                    startGPZone = row[7].ToString(),
                    endGPZone = row[8].ToString(),

                };
                list.Add(rootMaster);
            }

            sqlCommand.Dispose();
            sqlConn.Close();

            return list.ToArray();

            //return "gf";
        }

        public RepRootAllocationHeader[] getJsonRepRootAllocationHeader(string distributorId, string rep)
        {
            var list = new List<RepRootAllocationHeader>();

            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "Mobile_RepRootAllocation";


            sqlCommand.Parameters.Add(new SqlParameter("@CompID", distributorId));
            sqlCommand.Parameters.Add(new SqlParameter("@RepID ", rep));

            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                string hh = "hhh";
                var repRootAllocationHeader = new RepRootAllocationHeader()
                {
                    routeID = row[0].ToString(),
                    distributorID = row[1].ToString(),
                    repID = row[2].ToString(),
                    isSheduled = row[3].ToString(),
                    startDate = row[4].ToString(),
                    endDate = row[5].ToString()
                };
                list.Add(repRootAllocationHeader);
            }

            sqlCommand.Dispose();
            sqlConn.Close();

            return list.ToArray();


        }

        public string InsertJsonRepLocation(RepLocation repLocation)
        {
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var strResult = "ok";
            //string strPath = HostingEnvironment.MapPath("log.txt");
            //var RecFlag = "1";


            try
            {

                sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "Mobile_InsertRepLocation";

                // input param

                sqlCommand.Parameters.Add(new SqlParameter("@RepID", repLocation.RepID));
                sqlCommand.Parameters.Add(new SqlParameter("@AutoID", repLocation.AutoID));

                sqlCommand.Parameters.Add(new SqlParameter("@CompID", repLocation.CompID));
                sqlCommand.Parameters.Add(new SqlParameter("@Latitude", repLocation.Latitude));
                sqlCommand.Parameters.Add(new SqlParameter("@Longitude", repLocation.Longitude));
                sqlCommand.Parameters.Add(new SqlParameter("@DateTime", repLocation.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@BatteryLevel", repLocation.BatteryLevel));
                var returnString = sqlCommand.Parameters.Add("@RetID", SqlDbType.Int);

                returnString.Direction = ParameterDirection.Output;

                sqlConn.Open();

                sqlCommand.Connection = sqlConn;
                var intResult = sqlCommand.ExecuteNonQuery();
                strResult = returnString.Value.ToString();
                sqlConn.Close();
                //  file.WriteLine("strResult");
            }
            catch (Exception ex)
            {
                // strResult = "Error ";
                strResult = ex.Message;
                //  file.WriteLine(strResult);

            }

            // file.Close();
            return strResult;
        }



        public DocType[] getJsonDocType(String DistributorID)
        {
            var list = new List<DocType>();

            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "Mobile_GetDocType";
            sqlCommand.Parameters.Add(new SqlParameter("@Sel", 1));

            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                var docType = new DocType();

                docType.docTypeID = row[0].ToString();
                docType.docTypeCode = row[1].ToString();
                docType.docTypeDescription = row[2].ToString();


                list.Add(docType);
            }

            sqlCommand.Dispose();
            sqlConn.Close();

            return list.ToArray();
        }


        public PaymentMethod[] getJsonPaymentMethods(String DistributorID)
        {
            var list = new List<PaymentMethod>();

            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "Mobile_PaymentMethods";
            sqlCommand.Parameters.Add(new SqlParameter("@Sel", 1));

            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                var payementMethod = new PaymentMethod();

                payementMethod.PaymentMethodID = row[0].ToString();
                payementMethod.PaymentMethodCode = row[1].ToString();
                payementMethod.PaymentMethodDescription = row[2].ToString();


                list.Add(payementMethod);
            }

            sqlCommand.Dispose();
            sqlConn.Close();

            return list.ToArray();
        }

        public RepPrinterAllocation[] getJsonRepPrinterAllocation(string DistributorID, string rep)
        {

            var list = new List<RepPrinterAllocation>();
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "Mobile_RepPrinterAllocation";
            sqlCommand.Parameters.Add(new SqlParameter("@DID", DistributorID));
            sqlCommand.Parameters.Add(new SqlParameter("@RepID", rep));
            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                var repPrinterAllocation = new RepPrinterAllocation();

                repPrinterAllocation.repID = row[0].ToString();
                repPrinterAllocation.printerID = row[1].ToString();
                repPrinterAllocation.printerName = row[2].ToString();
                repPrinterAllocation.macAddress = row[3].ToString();
                repPrinterAllocation.displayName = row[4].ToString();
                repPrinterAllocation.cPLine = row[5].ToString();
                repPrinterAllocation.noOfCopies = row[6].ToString();
                repPrinterAllocation.noLinesPerPage = row[7].ToString();
                repPrinterAllocation.initLinePPgae = row[8].ToString();




                list.Add(repPrinterAllocation);
            }

            sqlCommand.Dispose();
            sqlConn.Close();

            return list.ToArray();
        }



        public FreeIssueList[] getJsonFreeissueList(string DistributorID, string repID)
        {
            var list = new List<FreeIssueList>();
            // var strSpilt = comp.Split('~');       




            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "Mobile_GetFreeIssueList";

            sqlCommand.Parameters.Add(new SqlParameter("@RepID", repID));
            sqlCommand.Parameters.Add(new SqlParameter("@DID", DistributorID));


            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);




            foreach (DataRow row in dt.Rows)
            {
                var fiPolicy = new FreeIssueList
                {

                    idFreeIssueList = row[0].ToString(),
                    iFreeIssueListNameID = row[1].ToString(),
                    iItemCatogeryID = row[2].ToString(),
                    iItemCatogery = row[3].ToString(),
                    iStockLink = row[4].ToString(),
                    iItemCode = row[5].ToString(),
                    iFreeIssueStockLink = row[6].ToString(),
                    iFreeIssueStockCode = row[7].ToString(),
                    iFreeIssueItemCatogery = row[8].ToString(),
                    iFrom = row[9].ToString(),
                    iTo = row[10].ToString(),
                    iQuantity = row[11].ToString(),
                    iExPrice = row[12].ToString(),
                    iIncPrice = row[13].ToString(),
                    pMethod = row[14].ToString(),
                    freeIssueType = row[15].ToString(),
                    iType = row[16].ToString(),
                    iTypeDescription = row[17].ToString(),
                    freeIssueTypeFlag = row[18].ToString(),
                    freeIssuepirority = row[19].ToString(),
                    freeissuemethod = row["freeissuemethod"].ToString(),
                    SubCatogoty = row["SubCatogoty"].ToString()

                };
                list.Add(fiPolicy);
            }

            sqlCommand.Dispose();
            sqlConn.Close();

            return list.ToArray();
        }

        public FreeIssueListName[] getJsonFreeIssueListName(string DistributorID, string repID)
        {
            var list = new List<FreeIssueListName>();



            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "Mobile_GetFreeIssueListName";

            sqlCommand.Parameters.Add(new SqlParameter("@RepID", DistributorID));
            sqlCommand.Parameters.Add(new SqlParameter("@DID", repID));


            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);




            foreach (DataRow row in dt.Rows)
            {
                var fiPolicy = new FreeIssueListName
                {

                    IDFreeIssueListName = row[0].ToString(),
                    cName = row[1].ToString(),
                    IsActive = row[2].ToString()
                };
                list.Add(fiPolicy);
            }

            sqlCommand.Dispose();
            sqlConn.Close();

            return list.ToArray();
        }


        public string InsertJsonUploadNewCustomerMaster(Customer customer)
        {
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var strResult = "ok";
            //string strPath = HostingEnvironment.MapPath("log.txt");
            //var RecFlag = "1";


            try
            {

                sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "Mobile_InsertCustomer";

                // input param


                sqlCommand.Parameters.Add(new SqlParameter("@AccountID", customer.AccountID));
                sqlCommand.Parameters.Add(new SqlParameter("@Code", customer.Code));
                sqlCommand.Parameters.Add(new SqlParameter("@Name", customer.Name));
                sqlCommand.Parameters.Add(new SqlParameter("@Latitude", customer.latitude));
                sqlCommand.Parameters.Add(new SqlParameter("@Longitude", customer.longitude));
                sqlCommand.Parameters.Add(new SqlParameter("@Address1", customer.Address1));
                sqlCommand.Parameters.Add(new SqlParameter("@Address2", customer.Address2));
                sqlCommand.Parameters.Add(new SqlParameter("@Address3", customer.Address3));
                sqlCommand.Parameters.Add(new SqlParameter("@ClassID", customer.ClassID));
                sqlCommand.Parameters.Add(new SqlParameter("@PriceListID", customer.PriceListID));
                sqlCommand.Parameters.Add(new SqlParameter("@RepID", customer.RepID));
                sqlCommand.Parameters.Add(new SqlParameter("@DID", customer.DID));
                sqlCommand.Parameters.Add(new SqlParameter("@CreditLimit", customer.CreditLimit));
                sqlCommand.Parameters.Add(new SqlParameter("@Telephone", customer.Telephone));
                sqlCommand.Parameters.Add(new SqlParameter("@Area", customer.Area));

                var returnString = sqlCommand.Parameters.Add("@Ret", SqlDbType.Int);

                returnString.Direction = ParameterDirection.Output;

                sqlConn.Open();

                sqlCommand.Connection = sqlConn;
                var intResult = sqlCommand.ExecuteNonQuery();
                strResult = returnString.Value.ToString();
                sqlConn.Close();
                //  file.WriteLine("strResult");
            }
            catch (Exception ex)
            {
                // strResult = "Error ";
                strResult = ex.Message;
                //  file.WriteLine(strResult);

            }

            // file.Close();
            return strResult;
        }

        public string InsertJsonUploadDayStartEnd(DayStartEnd dayStartEnd)
        {
            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var strResult = "ok";
            //string strPath = HostingEnvironment.MapPath("log.txt");
            //var RecFlag = "1";


            try
            {

                sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "Mobile_InsertDayStartEnd";

                // input param


                //sqlCommand.Parameters.Add(new SqlParameter("@AccountID", dayStartEnd.AccountID));
                sqlCommand.Parameters.Add(new SqlParameter("@RepID", dayStartEnd.RepId));
                sqlCommand.Parameters.Add(new SqlParameter("@TourId", dayStartEnd.TourId));
                sqlCommand.Parameters.Add(new SqlParameter("@StartEndDate", dayStartEnd.StartEndDate));
                sqlCommand.Parameters.Add(new SqlParameter("@Type", dayStartEnd.Type));
                sqlCommand.Parameters.Add(new SqlParameter("@CreadtedDateTime", dayStartEnd.CreadtedDateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@Notes", dayStartEnd.Notes));
                sqlCommand.Parameters.Add(new SqlParameter("@Issync", dayStartEnd.Issync));
                sqlCommand.Parameters.Add(new SqlParameter("@Latitude", dayStartEnd.Latitude));
                sqlCommand.Parameters.Add(new SqlParameter("@Longitude", dayStartEnd.Longitude));
                sqlCommand.Parameters.Add(new SqlParameter("@ExtraField1", dayStartEnd.ExtraField1));
                sqlCommand.Parameters.Add(new SqlParameter("@ExtraField2", dayStartEnd.ExtraField2));
                sqlCommand.Parameters.Add(new SqlParameter("@ExtraField3", dayStartEnd.ExtraField3));


                var returnString = sqlCommand.Parameters.Add("@Ret", SqlDbType.Int);

                returnString.Direction = ParameterDirection.Output;

                sqlConn.Open();

                sqlCommand.Connection = sqlConn;
                var intResult = sqlCommand.ExecuteNonQuery();
                strResult = returnString.Value.ToString();
                sqlConn.Close();
                //  file.WriteLine("strResult");
            }
            catch (Exception ex)
            {
                // strResult = "Error ";
                strResult = ex.Message;
                //  file.WriteLine(strResult);

            }

            // file.Close();
            return strResult;
        }



        public UOMMapping[] getJsonUOMMapping(string DistributorID, string repID)
        {
            var list = new List<UOMMapping>();

            var sqlCommand = new SqlCommand();
            var sqlConn = new SqlConnection();
            var dt = new DataTable();

            sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "Mobile_UOMMapping";
            sqlCommand.Parameters.Add(new SqlParameter("@RepID", repID));
            sqlCommand.Parameters.Add(new SqlParameter("@DistributorID", DistributorID));

            sqlConn.Open();
            sqlCommand.Connection = sqlConn;
            var da = new SqlDataAdapter(sqlCommand);
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                var uOMMapping = new UOMMapping();

                uOMMapping.baseUnitID = row["baseUnitID"].ToString();
                uOMMapping.uomID = row["unitID"].ToString();
                uOMMapping.baseUnitIDDescription = row["baseUnitCode"].ToString();
                uOMMapping.stockLink = row["itemID"].ToString();
                uOMMapping.uomDescription = row["unitCode"].ToString();
                uOMMapping.ration = row["ratio"].ToString();


                list.Add(uOMMapping);
            }

            sqlCommand.Dispose();
            sqlConn.Close();

            return list.ToArray();
        }


        public CalculatedQty[] GetCalculatedQty(string RepID, string CompID, String StockLink, String Qty)
        {
            try
            {
                var calculatedQtyList = new List<CalculatedQty>();
                var sqlCommand = new SqlCommand();
                var sqlConn = new SqlConnection();
                var dt = new DataTable();

                sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "Mobile_CalculateForQty";


                sqlCommand.Parameters.Add(new SqlParameter("@RepID", int.Parse(RepID)));
                sqlCommand.Parameters.Add(new SqlParameter("@CompanyID", int.Parse(CompID)));

                sqlCommand.Parameters.Add(new SqlParameter("@StockLink", int.Parse(StockLink)));
                sqlCommand.Parameters.Add(new SqlParameter("@Qty", float.Parse(Qty)));
                sqlConn.Open();
                sqlCommand.Connection = sqlConn;
                var da = new SqlDataAdapter(sqlCommand);
                da.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    var calculatedQty = new CalculatedQty();

                    calculatedQty.AvailableQty = row[0].ToString();
                    calculatedQty.FreeIssueQty = row[1].ToString();
                    calculatedQty.FIStockLink = row[2].ToString();

                    calculatedQtyList.Add(calculatedQty);
                }
                sqlCommand.Dispose();
                sqlConn.Close();
                return calculatedQtyList.ToArray();

            }
            catch (Exception ex)
            {
                return null;
                //  throw;
            }

        }


        public ProductGroupSequenceByOrder[] GetProductGroupSequenceByOrder(string RepID, string CompID)
        {
            try
            {
                var productGroupSequenceByOrderList = new List<ProductGroupSequenceByOrder>();
                var sqlCommand = new SqlCommand();
                var sqlConn = new SqlConnection();
                var dt = new DataTable();

                sqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["PerfectConnStr"].ToString();
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "Mobile_GetItemCategorySequencdByRep";


                sqlCommand.Parameters.Add(new SqlParameter("@RepID", int.Parse(RepID)));
                sqlCommand.Parameters.Add(new SqlParameter("@CompanyID", int.Parse(CompID)));
                sqlConn.Open();
                sqlCommand.Connection = sqlConn;
                var da = new SqlDataAdapter(sqlCommand);
                da.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    var productGroupSequenceByOrder = new ProductGroupSequenceByOrder();
                    productGroupSequenceByOrder.CategoryName = row[1].ToString();
                    productGroupSequenceByOrder.SequenceNumber = row[2].ToString();
                    productGroupSequenceByOrder.CategoryID = row[0].ToString();

                    productGroupSequenceByOrderList.Add(productGroupSequenceByOrder);
                }
                sqlCommand.Dispose();
                sqlConn.Close();
                return productGroupSequenceByOrderList.ToArray();

            }
            catch (Exception ex)
            {
                return null;
                //  throw;
            }

        }




        #endregion
    }
}
