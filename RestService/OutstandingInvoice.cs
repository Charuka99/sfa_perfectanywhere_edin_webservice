﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class OutstandingInvoice
    {
        [DataMember]
        public string InvID { get; set; }

        [DataMember]
        public string InvCode { get; set; }

        [DataMember]
        public string AccountID { get; set; }

        [DataMember]
        public string CusCode { get; set; }

        [DataMember]
        public string CusName { get; set; }

        [DataMember]
        public string InvoiceDate { get; set; }

        [DataMember]
        public string NetAmount { get; set; }

        [DataMember]
        public string Outstanding { get; set; }

        [DataMember]
        public string OrderNo { get; set; }

        [DataMember]
        public string MobileInvCode { get; set; }

        [DataMember]
        public string CompID { get; set; }
    }
}