﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class PriceList
    {
        [DataMember]
        public string PriceListID { get; set; }

        [DataMember]
        public string StockLink { get; set; }

        [DataMember]
        public string ExclPrice { get; set; }

        [DataMember]
        public string InclPrice { get; set; }
        [DataMember]
        public string TTIStatus { get; set; }

        [DataMember]
        public string TaxRateID { get; set; }
        [DataMember]
        public string LotID { get; set; }


    }
}