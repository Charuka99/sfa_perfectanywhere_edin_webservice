﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class RepLocation
    {
        [DataMember]
        public int RepID { get; set; }
        [DataMember]
        public int CompID { get; set; }
        [DataMember]
        public float Latitude { get; set; }
        [DataMember]
        public float Longitude { get; set; }
        [DataMember]
        public string DateTime { get; set; }
        [DataMember]
        public int StatusID { get; set; }

        [DataMember]
        public int AutoID { get; set; }

        [DataMember]
        public int BatteryLevel { get; set; }

    }
}