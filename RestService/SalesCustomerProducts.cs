﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class SalesCustomerProducts
    {
        [DataMember]
        public string CustomerCode { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public string ProductGroup { get; set; }

        [DataMember]
        public string ProductCode { get; set; }

        [DataMember]
        public string ProductDescription { get; set; }

        [DataMember]
        public string QTY { get; set; }

        [DataMember]
        public string VALUE { get; set; }

        [DataMember]
        public string TTI { get; set; }
    }
}