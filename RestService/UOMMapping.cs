﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class UOMMapping
    {
        [DataMember]
        public string stockLink { get; set; }

        [DataMember]
        public string baseUnitID { get; set; }

        [DataMember]
        public string uomID { get; set; }

        [DataMember]
        public string baseUnitIDDescription { get; set; }

        [DataMember]
        public string uomDescription { get; set; }

        [DataMember]
        public string ration { get; set; }




    }
}