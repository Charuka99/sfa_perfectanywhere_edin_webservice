﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class ProductWithLot
    {
        [DataMember]
        public string TransferDate { get; set; }

        [DataMember]
        public string TransferDescription { get; set; }

        [DataMember]
        public string TransferReference { get; set; }

        [DataMember]
        public string StockLink { get; set; }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string LotID { get; set; }

        [DataMember]
        public string LotDescription { get; set; }

        [DataMember]
        public string ExpiryDate { get; set; }

        [DataMember]
        public string WarehouseID { get; set; }

        [DataMember]
        public string QtyOnHand { get; set; }

        [DataMember]
        public string ProductGroup { get; set; }
        [DataMember]
        public string TTI { get; set; }
        [DataMember]
        public string ServiceItem  { get; set; }
        //itemactive

        [DataMember]
        public string SerialItem { get; set; }

        /*[DataMember]
        public string ItemActive { get; set; }*/

        [DataMember]
        public string WhseItem { get; set; }
        [DataMember]
       public string iLotStatus { get; set; }


        [DataMember]
        public string isSellableItem { get; set; }

        //pcid
      /*  [DataMember]
        public string PCID { get; set; }*/

        [DataMember]
        public string actualUOM { get; set; }


        [DataMember]
        public string SearchCode { get; set; }


        [DataMember]
        public string CaseMakeUp { get; set; }


        [DataMember]
        public string ItemSequence { get; set; }


        [DataMember]
        public string Barcode { get; set; }



    }
}