﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace RestService
{
     [DataContract(Namespace = "")]
    public class RepTarget
    {
         [DataMember]
         public string AccountNo { get; set; }
         [DataMember]
         public string Qty { get; set; }
         [DataMember]
         public string StockLink { get; set; }
         [DataMember]
         public string Target { get; set; }


        [DataMember]
        public string RecYear { get; set; }
        [DataMember]
        public string RecMonth { get; set; }

        [DataMember]
        public string RepCode { get; set; }  // catgory
        [DataMember]
        public string ItemCode { get; set; } //subcatogry

        [DataMember]
        public string trgt_date { get; set; }




    }
}