﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class DayStartEnd
    {
        [DataMember]
        public string RepId { get; set; }
        [DataMember]
        public string TourId { get; set; }
        [DataMember]
        public string StartEndDate { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string CreadtedDateTime { get; set; }
        [DataMember]
        public string Notes { get; set; }
        [DataMember]
        public string Issync { get; set; }
        [DataMember]
        public string Latitude { get; set; }
        [DataMember]
        public string Longitude { get; set; }
        [DataMember]
        public string ExtraField1 { get; set; }
        [DataMember]
        public string ExtraField2 { get; set; }
        [DataMember]
        public string ExtraField3 { get; set; }
    }
}