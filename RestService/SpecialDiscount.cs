﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace RestService
{
    public class SpecialDiscount
    {

        [DataMember]
        public string invoiceNo { get; set; }

        [DataMember]
        public string itemGroup { get; set; }

        [DataMember]
        public string itemGroupLink { get; set; }

        [DataMember]
        public string total { get; set; }

        [DataMember]
        public string dRate { get; set; }

      
    }
}