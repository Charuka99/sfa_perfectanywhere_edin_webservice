﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace RestService
{
       [DataContract(Namespace = "")]
    public class CancelInvResponse
    {
        [DataMember]
           public string compId { get; set; }

        [DataMember]
        public string repId { get; set; }

        [DataMember]
        public string invoiceNo { get; set; }


    }
}