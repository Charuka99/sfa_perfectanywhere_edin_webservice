﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class ShopAudit
    {


        [DataMember]
        public string ID { get; set; }
        [DataMember]
        public string ShAuditNum { get; set; }
        [DataMember]
        public string AuditDate { get; set; }
        [DataMember]
        public string DID { get; set; }
        [DataMember]
        public string Repid { get; set; }
        [DataMember]
        public string Repcode { get; set; }
        [DataMember]
        public string CID { get; set; }
        [DataMember]
        public string CCode { get; set; }
        [DataMember]
        public string CName { get; set; }
        [DataMember]
        public string ItemID { get; set; }
        [DataMember]
        public string ItemCode { get; set; }
        [DataMember]
        public string ItemName { get; set; }
        [DataMember]
        public string CountedQty { get; set; }
        [DataMember]
        public string SisMAvgsale { get; set; }
        [DataMember]
        public string LastmothsaleAqty { get; set; }
        [DataMember]
        public string SgusstedQty { get; set; }
        [DataMember]
        public string ActualQty { get; set; }





    }
}