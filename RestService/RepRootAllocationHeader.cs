﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class RepRootAllocationHeader
    {
        [DataMember]
        public string routeID { get; set; }
        [DataMember]
        public string distributorID { get; set; }
        [DataMember]
        public string repID { get; set; }
        [DataMember]
        public string isSheduled { get; set; }
        [DataMember]
        public string startDate { get; set; }
        [DataMember]
        public string endDate { get; set; }




    }
}