﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class InvoiceLineDisplay
    {
        [DataMember]
        public string CustomerCode { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public string InvoiceCode { get; set; }

        [DataMember]
        public string InvoiceDate { get; set; }

        [DataMember]
        public string HeaderGross { get; set; }

        [DataMember]
        public string HeaderDiscPerc { get; set; }

        [DataMember]
        public string HeaderNett { get; set; }

        [DataMember]
        public string ProductCode { get; set; }

        [DataMember]
        public string ProductDescription { get; set; }

        [DataMember]
        public string Qty { get; set; }

        [DataMember]
        public string InclPrice { get; set; }

        [DataMember]
        public string LineDiscPerc { get; set; }

        [DataMember]
        public string LineValue { get; set; }
    }
}