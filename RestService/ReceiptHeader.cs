﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class ReceiptHeader
    {
        [DataMember]
        public string RepID { get; set; }

        [DataMember]
        public string CompID { get; set; }

        [DataMember]
        public string DeviceID { get; set; }

        [DataMember]
        public string AutoID { get; set; }

        [DataMember]
        public string RecID { get; set; }

        [DataMember]
        public string ReceiptCode { get; set; }

        [DataMember]
        public string ReceiptDate { get; set; }

        [DataMember]
        public string AccountID { get; set; }

        [DataMember]
        public string CustomerCode { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public string PaymentMethod { get; set; }

        [DataMember]
        public string ChequeNo { get; set; }

        [DataMember]
        public string ChequeDate { get; set; }

        [DataMember]
        public string BankBranch { get; set; }

        [DataMember]
        public string Amount { get; set; }

        [DataMember]
        public string NumberOfLine { get; set; }

        [DataMember]
        public string RecFlag { get; set; }


        [DataMember]
        public string isReturncheque { get; set; }

        [DataMember(Name = "recd")]
        public List<ReceiptLine> recd { get; set; } = new List<ReceiptLine>();
    }
}