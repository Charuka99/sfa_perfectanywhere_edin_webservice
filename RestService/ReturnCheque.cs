﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class ReturnCheque
    {
        [DataMember]
        public string AccountId { get; set; }

        [DataMember]
        public string BankID { get; set; }

        [DataMember]
        public string ReturnChequeNumber { get; set; }

        [DataMember]
        public string RecieptCode { get; set; }


    }
}