﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class RequestData
    {
        [DataMember]
        public string CompID { get; set; }

        [DataMember]
        public string InvoiceCode { get; set; }
    }
}