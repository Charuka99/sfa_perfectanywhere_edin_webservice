﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace RestService
{
    [DataContract(Namespace = "")]

    public class BankMaster
    {
        [DataMember]
        public string BankID { get; set; }

        [DataMember]
        public string BankCode { get; set; }

        [DataMember]
        public string BankName { get; set; }
        
    }
}