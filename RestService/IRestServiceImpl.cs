﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace RestService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IRestServiceImpl" in both code and config file together.
    [ServiceContract]
    public interface IRestServiceImpl
    {
        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "xml/{id}")]
        string XMLData(string id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat  = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/UploadInvoiceHeader")]
        string UploadInvoiceHeader(InvoiceHeader invh);

        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/UploadInvoiceDetail")]
        string UploadInvoiceDetail(InvoiceLine invd);

        [OperationContract]
        [WebInvoke(Method = "GET",
    ResponseFormat = WebMessageFormat.Json,
    BodyStyle = WebMessageBodyStyle.Wrapped,
    UriTemplate = "json/getJsonInvoiceDetail/{distributorId}/{rep}")]
        InvoiceLine[] getJsonInvoiceDetail(string distributorId, string rep);


        [OperationContract]
        [WebInvoke(Method = "GET",
    ResponseFormat = WebMessageFormat.Json,
    BodyStyle = WebMessageBodyStyle.Wrapped,
    UriTemplate = "json/getJsonReturnCheque/{distributorId}/{rep}")]
        ReturnCheque[] getJsonReturnCheque(string distributorId, string rep);


        [OperationContract]
        [WebInvoke(Method = "GET",
    ResponseFormat = WebMessageFormat.Json,
    BodyStyle = WebMessageBodyStyle.Wrapped,
    UriTemplate = "json/getJsonBankMaster/{distributorId}/{rep}")]
        BankMaster[] getJsonBankMaster(string distributorId, string rep);



        [OperationContract]
        [WebInvoke(Method = "GET",
    ResponseFormat = WebMessageFormat.Json,
    BodyStyle = WebMessageBodyStyle.Wrapped,
    UriTemplate = "json/getJsonInvoiceLine/{distributorId}/{rep}")]
        InvoiceLine[] getJsonInvoiceLine(string distributorId, string rep);

        [OperationContract]
        [WebInvoke(Method = "GET",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "json/getJsonInvoiceHeader/{rep}/{distributorId}")]
        InvoiceHeader[] getJsonInvoiceHeader( string rep, string distributorId);


        [OperationContract]
        [WebInvoke(Method = "GET",
ResponseFormat = WebMessageFormat.Json,
BodyStyle = WebMessageBodyStyle.Wrapped,
UriTemplate = "json/getJsonHeadeDiscountScheme/{distributorId}/{rep}")]
        HeadeDiscountScheme[] getJsonHeadeDiscountScheme(string distributorId, string rep);


        [OperationContract]
        [WebInvoke(Method = "GET",
ResponseFormat = WebMessageFormat.Json,
BodyStyle = WebMessageBodyStyle.Wrapped,
UriTemplate = "json/getJsonDiscountSchemeList/{distributorId}/{rep}")]
        DiscountSchemeList[] getJsonDiscountSchemeList(string distributorId, string rep);


        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/getJsonCustomers/{distributorId}/{rep}")]
        Customer[] getJsonCustomers(string distributorId, string rep);


        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/getJsonNewCustomers/{distributorId}/{rep}")]
        Customer[] getJsonNewCustomers(string distributorId, string rep);

        [OperationContract]
        [WebInvoke(Method = "GET",
           ResponseFormat = WebMessageFormat.Json,
           BodyStyle = WebMessageBodyStyle.Wrapped,
           UriTemplate = "json/getJsonCompanyDetails/{distributorId}/{salesRep}")]
        CompanyDetails[] getJsonCompanyDetails(string distributorId , string salesRep);
  


        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/getJsonProducts/{comp}/{RepID}")]
        Product[] getJsonProducts(string comp, string RepID);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/getJsonProductsWithLot/{DistributorID}/{rep}")]
        ProductWithLot[] getJsonProductsWithLot(string DistributorID, string rep);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/getJsonFreeIssuePolicy/{comp}")]
        FreeIssuePolicy[] getJsonFreeIssuePolicy(string comp);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/getJsonPriceList/{DistributorID}/{rep}")]
        PriceList[] getJsonPriceList(string DistributorID, string rep);

        [OperationContract]
        [WebInvoke(Method = "GET", 
            BodyStyle = WebMessageBodyStyle.Wrapped, 
            UriTemplate = "json/getJsonSalesRep/{loginName}",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        SalesRep getJsonSalesRep(string loginName);

        
       
        [OperationContract]
        [WebInvoke(Method = "GET",
         ResponseFormat = WebMessageFormat.Json,
           BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "json/getJsonRepDetails/{DistributorID}/{repID}")]
        SalesRep[] getJsonRepDetails(string DistributorID, string repID);







        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/getJsonBusinessUnit/{loginName}/{DID}")]
        BusinessUnit[] getJsonBusinessUnit(string loginName,string DID);

        [OperationContract]
        [WebInvoke(Method = "GET",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/getJsonDownloadRecSummary/{repID}/{comp}",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        DownloadRecSummary getJsonDownloadRecSummary(string repID, string comp);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/getJsonInvoiceLineDisplay/{comp}/{inv}")]
        InvoiceLineDisplay[] getJsonInvoiceLineDisplay(string comp, string inv);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/getJsonOutstandingInvoices/{comp}/{rep}")]
        OutstandingInvoice[] getJsonOutstandingInvoices(string comp, string rep);

        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/SaveSalesReturn")]
        string SaveSalesReturn(RequestData rdata);

        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/UploadReceiptHeader")]
        string UploadReceiptHeader(ReceiptHeader rech);

        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/UploadReceiptHeader2")]
        string UploadReceiptHeader2(ReceiptHeader rech);

        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/UploadReceiptLine")]
        string UploadReceiptLine(ReceiptLine recd);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/getJsonSalesCustomerProducts/{Sel}/{CompID}/{CusID}/{RepID}/{StockID}/{FromDate}/{ToDate}")]
        SalesCustomerProducts[] getJsonSalesCustomerProducts(string Sel, string CompID, string CusID, string RepID, string StockID, string FromDate, string ToDate);
        //[OperationContract]
        //[WebInvoke(Method = "GET",
        //    ResponseFormat = WebMessageFormat.Json,
        //    BodyStyle = WebMessageBodyStyle.Wrapped,
        //    UriTemplate = "json/getJsonSalesCustomerProducts/{Sel}/{CompID}/{CusID}/{RepID}/{StockID}/{FromDate}/{ToDate}")]
        //string getJsonSalesCustomerProducts(string Sel, string CompID, string CusID, string RepID, string StockID, string FromDate, string ToDate);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/GetJsonRepTarget/{CompID}/{RepID}/{Year}/{Month}")]
        RepTarget[] GetJsonRepTarget(string CompID, string RepID,string Year,string Month);



        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/getJsonTaxRate")]
        Tax[] getJsonTaxRate();

        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/InsertSpecialDiscount")]
        string InsertSpecialDiscount(SpecialDiscount special);

        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/InsertLot")]
        string InsertLot(Lot invLot);


        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/getJsonCInvResult/{CompID}/{RepID}")]
        MobileCancelInv[] getJsonCInvResult(string CompID, string RepID);

        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/UpdateCancelInvResponse")]
        string UpdateCancelInvResponse(CancelInvResponse CInvResponse);


        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/JsonBanking")]
        string InsertJsonBanking(BankDeposit bpDeposit);


        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/JsonCustomerVisit")]
        string InsertJsonCustomerVisit(CustomerVisit CVisit);



        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/JsonMeterReading")]
        string InsertJsonMeaterReading(MeterReading CVisit);



        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "json/JsonShopAuditData")]
        string InsertJsonnsertShopAuditData(ShopAudit CVisit);



        [OperationContract]
        [WebInvoke(Method = "GET",
           ResponseFormat = WebMessageFormat.Json,
           BodyStyle = WebMessageBodyStyle.Wrapped,
           UriTemplate = "json/getJsonRootMaster/{DistributorID}/{rep}")]
        RootMaster[] getJsonRootMaster(string DistributorID, string rep);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/getJsonRepRootAllocationHeader/{DistributorID}/{rep}")]
        RepRootAllocationHeader[] getJsonRepRootAllocationHeader(string DistributorID, string rep);



        [OperationContract]
        [WebInvoke(Method = "POST",
          ResponseFormat = WebMessageFormat.Json,
          BodyStyle = WebMessageBodyStyle.Wrapped,
          UriTemplate = "json/setJsonRepLocation")]
        string InsertJsonRepLocation(RepLocation repLocation);




        [OperationContract]
        [WebInvoke(Method = "GET",
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.Wrapped,
       UriTemplate = "json/getJsonDocType/{DistributorID}")]
        DocType[] getJsonDocType(string DistributorID);

        [OperationContract]
        [WebInvoke(Method = "GET",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "json/getJsonPaymentMethods/{DistributorID}")]
        PaymentMethod[] getJsonPaymentMethods(string DistributorID);


        [OperationContract]
        [WebInvoke(Method = "GET",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "json/getJsonRepPrinterAllocation/{DistributorID}/{repID}")]
        RepPrinterAllocation[] getJsonRepPrinterAllocation(string DistributorID,string repID);




        [OperationContract]
        [WebInvoke(Method = "GET",
ResponseFormat = WebMessageFormat.Json,
BodyStyle = WebMessageBodyStyle.Wrapped,
UriTemplate = "json/getJsonFreeIssueList/{DistributorID}/{repID}")]
        FreeIssueList[] getJsonFreeissueList(string DistributorID, string repID);


        [OperationContract]
        [WebInvoke(Method = "GET",
ResponseFormat = WebMessageFormat.Json,
BodyStyle = WebMessageBodyStyle.Wrapped,
UriTemplate = "json/getJsonFreeIssueListName/{DistributorID}/{repID}")]
        FreeIssueListName[] getJsonFreeIssueListName(string DistributorID, string repID);


        [OperationContract]
        [WebInvoke(Method = "POST",
    ResponseFormat = WebMessageFormat.Json,
    BodyStyle = WebMessageBodyStyle.Wrapped,
    UriTemplate = "json/UploadDayStartEnd")]
        string InsertJsonUploadDayStartEnd(DayStartEnd dayStartEnd);

        [OperationContract]
        [WebInvoke(Method = "POST",
    ResponseFormat = WebMessageFormat.Json,
    BodyStyle = WebMessageBodyStyle.Wrapped,
    UriTemplate = "json/UploadNewCustomerMaster")]
        string InsertJsonUploadNewCustomerMaster(Customer customerMaster);

                    [OperationContract]
                    [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/getJsonUOMMapping/{DistributorID}/{repID}")]
                    UOMMapping[] getJsonUOMMapping(string DistributorID, string repID);


        [OperationContract]
        [WebInvoke(Method = "GET",
           ResponseFormat = WebMessageFormat.Json,
           BodyStyle = WebMessageBodyStyle.Wrapped,
           UriTemplate = "json/GetProductGroupSequenceByOrder/{RepID}/{CompID}")]
        ProductGroupSequenceByOrder[] GetProductGroupSequenceByOrder(string RepID, string CompID);


        [OperationContract]
        [WebInvoke(Method = "GET",
ResponseFormat = WebMessageFormat.Json,
BodyStyle = WebMessageBodyStyle.Wrapped,
UriTemplate = "json/GetCalculatedQty/{RepID}/{CompID}/{StockLink}/{Qty}")]
        CalculatedQty[] GetCalculatedQty(string RepID, string CompID, String StockLink, String Qty);




    }
}
 