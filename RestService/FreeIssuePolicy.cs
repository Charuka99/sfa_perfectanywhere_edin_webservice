﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class FreeIssuePolicy
    {
        [DataMember]
        public string AutoID { get; set; }

        [DataMember]
        public string ScheemCode { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string ScheemType { get; set; }

        [DataMember]
        public string ItemLink { get; set; }

        [DataMember]
        public string ItemCode { get; set; }

        [DataMember]
        public string ItemDescription { get; set; }

        [DataMember]
        public string Quantity { get; set; }

        [DataMember]
        public string ItemLinkFI { get; set; }

        [DataMember]
        public string ItemCodeFI { get; set; }

        [DataMember]
        public string ItemDescriptionFI { get; set; }

        [DataMember]
        public string FreeIssueQuantity { get; set; }

          [DataMember]
        public string SelectionType { get; set; }
           [DataMember]
        public string PaymentMethod { get; set; }
          [DataMember]
        public string DCLink { get; set; }        	
	      [DataMember]
        public string ItemGroupLink { get; set; }		
		  [DataMember]
        public string ItemGroup { get; set; }

        [DataMember]
        public string classID { get; set; }


    }
}