﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace RestService
{
    [DataContract(Namespace = "")]
    public class CalculatedQty
    {
        [DataMember]
        public string RepID { get; set; }

        [DataMember]
        public string CompanyID { get; set; }

        [DataMember]
        public string FIStockLink { get; set; }

        [DataMember]
        public string AvailableQty { get; set; }


        [DataMember]
        public string FreeIssueQty { get; set; }








    }
}