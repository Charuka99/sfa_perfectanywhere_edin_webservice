﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace RestService
{
     [DataContract(Namespace = "")]
    public class RootMaster
    {


        [DataMember]
        public string routeID { get; set; }
        [DataMember]
        public string distributorID { get; set; }
        [DataMember]
        public string repID { get; set; }
        [DataMember]
        public string routeCode { get; set; }
        [DataMember]
        public string routeDescription { get; set; }
       [DataMember]
        public string parentRouteID { get; set; }
       [DataMember]
       public string recFlag { get; set; }
       [DataMember]
       public string startGPZone { get; set; }
       [DataMember]
        public string endGPZone { get; set; }


     

    }
}