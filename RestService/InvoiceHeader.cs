﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class InvoiceHeader
    {
        [DataMember]
        public string RepID { get; set; }

        [DataMember]
        public string CompID { get; set; }

        [DataMember]
        public string DeviceID { get; set; }

        [DataMember]
        public string AutoID { get; set; }

        [DataMember]
        public string RecID { get; set; }

        [DataMember]
        public string InvoiceCode { get; set; }

        [DataMember]
        public string OrderDate { get; set; }

        [DataMember]
        public string InvoiceDate { get; set; }

        [DataMember]
        public string AccountID { get; set; }

        [DataMember]
        public string ProjectID { get; set; }

        [DataMember]
        public string HeaderDiscPerc { get; set; }

        [DataMember]
        public string ExclNoDisc { get; set; }

        [DataMember]
        public string InclNoDisc { get; set; }

        [DataMember]
        public string TaxAmount { get; set; }

        [DataMember]
        public string ExclWithDisc { get; set; }

        [DataMember]
        public string InclWithDisc { get; set; }

        [DataMember]
        public string SalesType { get; set; }

        [DataMember]
        public string NumberOfLine { get; set; }

        [DataMember]
        public string TrnYear { get; set; }

        [DataMember]
        public string TrnMonth { get; set; }

        [DataMember]
        public string TrnDay { get; set; }

        [DataMember]
        public string RecFlag { get; set; }
        [DataMember]
        public string pMethode { get; set; }

        [DataMember]
        public string latitude { get; set; }
        [DataMember]
        public string longitude { get; set; }
        [DataMember]
        public string ProcessedDate { get; set; }

        [DataMember]
        public int isHeaderDiscEdited { get; set; }
        [DataMember]
        public float headerDiscRate { get; set; }

        [DataMember]
        public int AID { get; set; }

        [DataMember]
        public string isQRVerified { get; set; }

        [DataMember]
        public string ExtratStringfiled1 { get; set; } //this is cus invoice remark

        [DataMember]
        public string ProcessInvCode { get; set; }

        [DataMember]
        public string HeaderDiscon { get; set; }

        [DataMember]
        public string BankName { get; set; } //this is cus invoice remark

        [DataMember]
        public string cheque { get; set; }

        [DataMember]
        public string chequeDate { get; set; }

        [DataMember]
        public string BatLevel { get; set; }


        [DataMember]
        public string EndDatetime { get; set; }


        [DataMember]
        public string linksonumber { get; set; }

        [DataMember]
        public string ErlyOutoftheroueRemark { get; set; }

        [DataMember]
        public string IspriceListChange { get; set; }
        

        [DataMember(Name = "invd")]
        public List<InvoiceLine> invoiceLines { get; set; } = new List<InvoiceLine>();

        //public List<InvoiceLine> invoiceLines;






    }
}