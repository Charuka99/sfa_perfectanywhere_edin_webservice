﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace RestService
{
    public class Tax
    {
        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string TaxRate { get; set; }

        [DataMember]
        public string TaxRate_iBranchID { get; set; }
    }
}