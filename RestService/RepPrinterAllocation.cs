﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class RepPrinterAllocation
    {
        [DataMember]

        public string repID { get; set; }
        [DataMember]

        public string printerID { get; set; }
        [DataMember]
        public string printerName { get; set; }
        [DataMember]
        public string macAddress { get; set; }
        [DataMember]
        public string displayName { get; set; }
        [DataMember]
        public string cPLine { get; set; }
        public string noOfCopies { get; set; }
        [DataMember]
        public string noLinesPerPage { get; set; }
        [DataMember]
        public string initLinePPgae { get; set; }


    }
}