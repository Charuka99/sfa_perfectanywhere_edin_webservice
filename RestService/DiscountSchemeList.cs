﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]

    public class DiscountSchemeList
    {

        [DataMember]
        public string HeaderDisID { get; set; }
        [DataMember]
        public string HeaderDisDetailID { get; set; }
        [DataMember]
        public string ItemCatogory { get; set; }
        [DataMember]
        public string StockLink { get; set; }
        [DataMember]
        public string ItemCode { get; set; }
        [DataMember]
        public string DiscountStockLink { get; set; }
        [DataMember]
        public string DiscountItemCode { get; set; }
        [DataMember]
        public string DiscountItemCatogory { get; set; }
        [DataMember]
        public string FromVal { get; set; }
        [DataMember]
        public string Toval { get; set; }
        [DataMember]
        public string DiscountValue { get; set; }
        [DataMember]
        public string BaseType { get; set; }
        [DataMember]
        public string ModifyDate { get; set; }
        [DataMember]
        public string DID { get; set; }
        [DataMember]
        public string Active { get; set; }
        
    }
}