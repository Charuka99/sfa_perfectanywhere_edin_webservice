﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace RestService
{
        [DataContract(Namespace = "")]
    public class BankDeposit
    {
        [DataMember]
        public string amount { get; set; }

        [DataMember]
        public string recieptNo { get; set; }

        [DataMember]
        public string comment { get; set; }

        [DataMember]
        public string type { get; set; }
        [DataMember]
        public string date { get; set; }
        [DataMember]
        public string RepID { get; set; }
        [DataMember]
        public string ComID { get; set; }
        [DataMember]
        public string TabID { get; set; }
    }
}