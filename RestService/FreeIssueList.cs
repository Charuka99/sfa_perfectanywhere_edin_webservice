﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class FreeIssueList
    {

        
        [DataMember]
        public string idFreeIssueList { get; set; }
        [DataMember]
        public string iFreeIssueListNameID { get; set; }
        [DataMember]
        public string iItemCatogeryID { get; set; }
        [DataMember]
        public string iItemCatogery { get; set; }
        [DataMember]
        public string iStockLink { get; set; }
        [DataMember]
        public string iItemCode { get; set; }
        [DataMember]
        public string iFreeIssueStockLink { get; set; }
        [DataMember]
        public string iFreeIssueStockCode { get; set; }
        [DataMember]
        public string iFreeIssueItemCatogery { get; set; }
        [DataMember]
        public string iFrom { get; set; }
        [DataMember]
        public string iTo { get; set; }
        [DataMember]
        public string iQuantity { get; set; }
        [DataMember]
        public string iExPrice { get; set; }
        [DataMember]
        public string iIncPrice { get; set; }
        [DataMember]
        public string pMethod { get; set; }
        [DataMember]
        public string freeIssueType { get; set; }
        [DataMember]
        public string iType { get; set; }
        [DataMember]
        public string iTypeDescription { get; set; }

        [DataMember]
        public string freeIssueTypeFlag { get; set; }

        [DataMember]
        public string freeIssuepirority { get; set; }

        [DataMember]
        public string freeissuemethod { get; set; }

        [DataMember]
        public string SubCatogoty { get; set; }

     





    }
}