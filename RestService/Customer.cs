﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace RestService
{
    [DataContract(Namespace = "")]
    public class Customer
    {
        [DataMember]
        public string AccountID { get; set; }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string Name 
        { get; set; }

        [DataMember]
        public string Address1 { get; set; }

        [DataMember]
        public string Address2 { get; set; }

        [DataMember]
        public string Address3 { get; set; }

        [DataMember]
        public string ClassID { get; set; }

        [DataMember]
        public string PriceListID { get; set; }

        [DataMember]
        public string RepID { get; set; }

        [DataMember]
        public string Outstanding { get; set; }

        [DataMember]
        public string CreditLimit { get; set; }

        [DataMember]
        public string CreditDays { get; set; }

        [DataMember]
        public string Area { get; set; }

        [DataMember]
        public string ulARTaxStatus { get; set; }

        [DataMember]
        public string IsFreeIssue { get; set; }
        [DataMember]
        public string IsDiscount { get; set; }


        [DataMember]
        public string cLimitStatusSO { get; set; }
        [DataMember]
        public string cPeriodStatusSO { get; set; }
        [DataMember]
        public string availableQtyConStatusSO { get; set; }
        [DataMember]
        public string dPaymentStatusSO { get; set; }
        [DataMember]
        public string recieptAvailableSO { get; set; }
        //  private int printrecieptAvailableSO=0;
        [DataMember]
        public string cLimitStatusSI { get; set; }
        [DataMember]
        public string cPeriodStatusSI { get; set; }
        [DataMember]
        public string availableQtyConStatusSI { get; set; }
        [DataMember]
        public string dPaymentStatusSI { get; set; }
        [DataMember]
        public string recieptAvailableSI { get; set; }


        [DataMember]
        public string creditPeriod { get; set; }
        [DataMember]
        public string latitude { get; set; }

        [DataMember]
        public string longitude { get; set; }





        [DataMember]
        public string isNewCustomer { get; set; }
        [DataMember]
        public string fIListID { get; set; }


        [DataMember]
        public string DID { get; set; }

        [DataMember]
        public string OutsatingOverDue { get; set; }

        [DataMember]
        public string Rootpirority { get; set; }

        [DataMember]
        public string PDChequegracePeriod { get; set; }

        [DataMember]
        public string Telephone { get; set; }

        [DataMember]
        public string contact { get; set; }



        [DataMember]
        public string PaymentType { get; set; }


        [DataMember]
        public string Ishold { get; set; }


        [DataMember]
        public string TAXNumber { get; set; }


        [DataMember]
        public string TAXStuts { get; set; }

        [DataMember]
        public string customerType { get; set; }




    }
}